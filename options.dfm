object Form1: TForm1
  Left = 345
  Top = 129
  BorderStyle = bsSingle
  Caption = 'Options'
  ClientHeight = 567
  ClientWidth = 521
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object OpsPages: TPageControl
    Left = 0
    Top = 0
    Width = 521
    Height = 529
    ActivePage = AYEmu
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object CurWinds: TTabSheet
      Caption = 'Main'
      ImageIndex = 2
      object PatEditorOpts: TGroupBox
        Left = 8
        Top = 8
        Width = 497
        Height = 193
        Caption = ' Pattern Editor Options '
        TabOrder = 0
        object DecNumbersLines: TCheckBox
          Left = 12
          Top = 30
          Width = 295
          Height = 17
          Hint = 'Delete Position'
          Caption = 'Decimal for Line Numbers in pattern, sample, ornament'
          TabOrder = 0
          OnClick = DecNumbersLinesClick
        end
        object DecNumbersNoise: TCheckBox
          Left = 12
          Top = 62
          Width = 287
          Height = 17
          Caption = 'Decimal for Noise Level in pattern and sample '
          TabOrder = 1
          OnClick = DecNumbersNoiseClick
        end
        object chkHS: TCheckBox
          Left = 12
          Top = 94
          Width = 287
          Height = 17
          Hint = 'Delete Position'
          Caption = 'Highlight Speed-rel positions for sample and ornament'
          TabOrder = 2
          OnClick = chkHSClick
        end
        object DisablePatSeparators: TCheckBox
          Left = 12
          Top = 126
          Width = 273
          Height = 17
          Caption = 'Disable vertical pattern separators'
          TabOrder = 3
          OnClick = DisablePatSeparatorsClick
        end
        object DisableHints: TCheckBox
          Left = 12
          Top = 158
          Width = 279
          Height = 17
          Caption = 'Disable Hints in pattern, samples and ornaments editor'
          TabOrder = 4
          OnClick = DisableHintsClick
        end
      end
      object BackupOpts: TGroupBox
        Left = 264
        Top = 216
        Width = 241
        Height = 153
        Caption = ' Backup Options '
        TabOrder = 1
        object Label16: TLabel
          Left = 12
          Top = 66
          Width = 30
          Height = 13
          Caption = 'Every:'
        end
        object Label17: TLabel
          Left = 104
          Top = 66
          Width = 36
          Height = 13
          Caption = 'minutes'
        end
        object Label18: TLabel
          Left = 10
          Top = 103
          Width = 191
          Height = 13
          Caption = 'Backup filename: "filename ver 001.vt2"'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object AutoSaveBackups: TCheckBox
          Left = 12
          Top = 30
          Width = 145
          Height = 17
          Caption = 'Autosave Backups'
          TabOrder = 0
          OnMouseUp = AutoSaveBackupsMouseUp
        end
        object BackupsMinsVal: TEdit
          Left = 50
          Top = 63
          Width = 33
          Height = 21
          TabOrder = 1
          Text = '1'
          OnChange = BackupsMinsValChange
        end
        object BackupEveryMins: TUpDown
          Left = 83
          Top = 63
          Width = 14
          Height = 21
          Associate = BackupsMinsVal
          Min = 1
          Max = 30
          Position = 1
          TabOrder = 2
        end
      end
      object PriorGrp: TRadioGroup
        Left = 264
        Top = 384
        Width = 241
        Height = 105
        Caption = 'Application Priority  '
        ItemIndex = 0
        Items.Strings = (
          'Normal'
          'High')
        TabOrder = 2
        OnClick = PriorGrpClick
      end
      object StartupBox: TGroupBox
        Left = 8
        Top = 216
        Width = 241
        Height = 153
        Caption = ' Startup '
        TabOrder = 3
        object start1: TLabel
          Left = 12
          Top = 30
          Width = 93
          Height = 13
          Caption = 'When Vortex starts:'
        end
        object TemplPathLab: TLabel
          Left = 12
          Top = 82
          Width = 75
          Height = 13
          Caption = 'Template Song:'
        end
        object StartsAction: TComboBox
          Left = 12
          Top = 47
          Width = 189
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = StartsActionChange
          Items.Strings = (
            'Open template'
            'Open blank song'
            'Do nothing')
        end
        object TemplateSong: TEdit
          Left = 12
          Top = 99
          Width = 162
          Height = 21
          TabOrder = 1
        end
        object BrowseTemplate: TButton
          Left = 176
          Top = 99
          Width = 25
          Height = 21
          Caption = '...'
          TabOrder = 2
          OnClick = BrowseTemplateClick
        end
      end
      object FreqTableBox: TGroupBox
        Left = 8
        Top = 384
        Width = 241
        Height = 105
        Caption = ' Frequency Table '
        TabOrder = 4
        object Label15: TLabel
          Left = 12
          Top = 30
          Width = 120
          Height = 13
          Hint = 'Default Frequency Table for new tracks'
          Caption = 'Default Frequency Table:'
          ParentShowHint = False
          ShowHint = True
        end
        object TableName: TLabel
          Left = 11
          Top = 83
          Width = 114
          Height = 13
          Caption = 'ASM or PSC (1.75 MHz)'
        end
        object DefaultTable: TEdit
          Left = 12
          Top = 55
          Width = 31
          Height = 21
          ReadOnly = True
          TabOrder = 0
          Text = '2'
          OnChange = DefaultTableChange
        end
        object UpDown2: TUpDown
          Left = 43
          Top = 55
          Width = 14
          Height = 21
          Associate = DefaultTable
          Max = 4
          Position = 2
          TabOrder = 1
        end
      end
    end
    object ColorThemesTab: TTabSheet
      Caption = 'Apperance'
      ImageIndex = 5
      object GroupBox3: TGroupBox
        Left = 8
        Top = 8
        Width = 225
        Height = 145
        Caption = ' Color Themes '
        TabOrder = 0
        object ColorThemesList: TListBox
          Left = 8
          Top = 24
          Width = 145
          Height = 112
          ItemHeight = 13
          TabOrder = 0
          OnClick = ColorThemesListClick
        end
        object BtnLoadTheme: TButton
          Left = 159
          Top = 24
          Width = 58
          Height = 21
          Caption = 'Load'
          TabOrder = 1
          OnClick = BtnLoadThemeClick
        end
        object BtnSaveTheme: TButton
          Left = 159
          Top = 47
          Width = 58
          Height = 21
          Caption = 'Save'
          TabOrder = 2
          OnClick = BtnSaveThemeClick
        end
        object BtnDelTheme: TButton
          Left = 159
          Top = 116
          Width = 58
          Height = 21
          Caption = 'Delete'
          TabOrder = 3
          OnClick = BtnDelThemeClick
        end
        object BtnCloneTheme: TButton
          Left = 159
          Top = 70
          Width = 58
          Height = 21
          Caption = 'Duplicate'
          TabOrder = 4
          OnClick = BtnCloneThemeClick
        end
        object BtnRenameTheme: TButton
          Left = 159
          Top = 93
          Width = 58
          Height = 21
          Caption = 'Rename'
          TabOrder = 5
          OnClick = BtnRenameThemeClick
        end
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 160
        Width = 497
        Height = 337
        Caption = ' Color Theme Options  '
        TabOrder = 1
        object TableBottom: TShape
          Left = 8
          Top = 48
          Width = 481
          Height = 281
          Brush.Color = 16382455
          Pen.Color = 13290186
        end
        object Shape1: TShape
          Left = 8
          Top = 304
          Width = 481
          Height = 25
          Brush.Color = 15461355
          Pen.Color = 13290186
        end
        object BG10: TShape
          Left = 9
          Top = 272
          Width = 480
          Height = 25
          Brush.Color = 14737632
          Pen.Style = psClear
        end
        object BG1: TShape
          Left = 9
          Top = 56
          Width = 480
          Height = 25
          Brush.Color = 15461355
          Pen.Style = psClear
        end
        object TableHeader: TShape
          Left = 8
          Top = 24
          Width = 481
          Height = 25
          Brush.Color = 16382455
          Pen.Color = 13290186
        end
        object ColBackground: TShape
          Left = 144
          Top = 59
          Width = 25
          Height = 18
          Hint = 'Main Background'
          Brush.Color = 2894892
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColBackgroundMouseDown
        end
        object LDefinition: TLabel
          Left = 20
          Top = 30
          Width = 44
          Height = 13
          Caption = 'Definition'
          Color = 16382455
          ParentColor = False
        end
        object LCurrPat: TLabel
          Left = 144
          Top = 30
          Width = 70
          Height = 13
          Caption = 'Current pattern'
          Color = 16382455
          ParentColor = False
        end
        object LNextPrevPat: TLabel
          Left = 264
          Top = 30
          Width = 85
          Height = 13
          Caption = 'Next/Prev pattern'
          Color = 16382455
          ParentColor = False
        end
        object LBackgr: TLabel
          Left = 20
          Top = 61
          Width = 69
          Height = 13
          Caption = 'Background'
          Color = 15461355
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object BG2: TShape
          Left = 9
          Top = 80
          Width = 480
          Height = 25
          Brush.Color = 14737632
          Pen.Style = psClear
        end
        object LText: TLabel
          Left = 20
          Top = 85
          Width = 26
          Height = 13
          Caption = 'Text'
          Color = 14737632
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object BG3: TShape
          Left = 9
          Top = 104
          Width = 480
          Height = 25
          Brush.Color = 15461355
          Pen.Style = psClear
        end
        object ColText: TShape
          Left = 144
          Top = 83
          Width = 25
          Height = 18
          Hint = 'Text (Dots)'
          Brush.Color = 15658734
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColTextMouseDown
        end
        object BG4: TShape
          Left = 9
          Top = 128
          Width = 480
          Height = 25
          Brush.Color = 14737632
          Pen.Style = psClear
        end
        object BG5: TShape
          Left = 9
          Top = 152
          Width = 480
          Height = 25
          Brush.Color = 15461355
          Pen.Style = psClear
        end
        object BG6: TShape
          Left = 9
          Top = 176
          Width = 480
          Height = 25
          Brush.Color = 14737632
          Pen.Style = psClear
        end
        object LLineNumbs: TLabel
          Left = 20
          Top = 109
          Width = 76
          Height = 13
          Caption = 'Line numbers'
          Color = 15461355
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object LEnvelope: TLabel
          Left = 20
          Top = 133
          Width = 54
          Height = 13
          Caption = 'Envelope'
          Color = 14737632
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object LNoise: TLabel
          Left = 20
          Top = 157
          Width = 33
          Height = 13
          Caption = 'Noise'
          Color = 15461355
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object LNote: TLabel
          Left = 20
          Top = 181
          Width = 28
          Height = 13
          Caption = 'Note'
          Color = 14737632
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object BG7: TShape
          Left = 9
          Top = 200
          Width = 480
          Height = 25
          Brush.Color = 15461355
          Pen.Style = psClear
        end
        object BG8: TShape
          Left = 9
          Top = 224
          Width = 480
          Height = 25
          Brush.Color = 14737632
          Pen.Style = psClear
        end
        object BG9: TShape
          Left = 9
          Top = 248
          Width = 480
          Height = 25
          Brush.Color = 15461355
          Pen.Style = psClear
        end
        object LNoteParams: TLabel
          Left = 20
          Top = 205
          Width = 72
          Height = 13
          Caption = 'Note params'
          Color = 15461355
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object LNoteCommands: TLabel
          Left = 20
          Top = 229
          Width = 91
          Height = 13
          Caption = 'Note commands'
          Color = 14737632
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object ColSelLineBackground: TShape
          Left = 176
          Top = 59
          Width = 25
          Height = 18
          Hint = 'Selected Line Background'
          Brush.Color = 11053224
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSelLineBackgroundMouseDown
        end
        object ColHighlBackground: TShape
          Left = 208
          Top = 59
          Width = 25
          Height = 18
          Hint = 'Highlighted Line Background'
          Brush.Color = 4868682
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColHighlBackgroundMouseDown
        end
        object ColHighlText: TShape
          Left = 208
          Top = 83
          Width = 25
          Height = 18
          Hint = 'Highlighted Text (Dots)'
          Brush.Color = 15263976
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColHighlTextMouseDown
        end
        object ColLineNum: TShape
          Left = 144
          Top = 107
          Width = 25
          Height = 18
          Hint = 'Line Numbers'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColLineNumMouseDown
        end
        object ColEnvelope: TShape
          Left = 144
          Top = 131
          Width = 25
          Height = 18
          Hint = 'Envelope'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColEnvelopeMouseDown
        end
        object ColNoise: TShape
          Left = 144
          Top = 155
          Width = 25
          Height = 18
          Hint = 'Noise'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColNoiseMouseDown
        end
        object ColNote: TShape
          Left = 144
          Top = 179
          Width = 25
          Height = 18
          Hint = 'Note'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColNoteMouseDown
        end
        object ColNoteParams: TShape
          Left = 144
          Top = 203
          Width = 25
          Height = 18
          Hint = 'Note Params (Sample, Envelope, Ornament, Volume)'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColNoteParamsMouseDown
        end
        object ColNoteCommands: TShape
          Left = 144
          Top = 227
          Width = 25
          Height = 18
          Hint = 'Special Note Commands'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColNoteCommandsMouseDown
        end
        object ColOutBackground: TShape
          Left = 264
          Top = 59
          Width = 25
          Height = 18
          Hint = 'Main Background'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColOutBackgroundMouseDown
        end
        object ColOutText: TShape
          Left = 264
          Top = 83
          Width = 25
          Height = 18
          Hint = 'Text'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColOutTextMouseDown
        end
        object ColOutHlBackground: TShape
          Left = 296
          Top = 59
          Width = 25
          Height = 18
          Hint = 'Highlighted Line Background'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColOutHlBackgroundMouseDown
        end
        object LSeparators: TLabel
          Left = 20
          Top = 253
          Width = 62
          Height = 13
          Caption = 'Separators'
          Color = 15461355
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object ColSeparators: TShape
          Left = 144
          Top = 251
          Width = 25
          Height = 18
          Hint = 'Vertical Separators'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSeparatorsMouseDown
        end
        object Sep1: TShape
          Left = 128
          Top = 51
          Width = 1
          Height = 251
          Pen.Color = 12171705
        end
        object Sep2: TShape
          Left = 125
          Top = 51
          Width = 1
          Height = 251
          Pen.Color = 12171705
        end
        object Sep3: TShape
          Left = 247
          Top = 51
          Width = 1
          Height = 251
          Pen.Color = 12171705
        end
        object Sep4: TShape
          Left = 247
          Top = 27
          Width = 1
          Height = 19
          Pen.Color = 13290186
        end
        object Sep5: TShape
          Left = 125
          Top = 27
          Width = 1
          Height = 19
          Pen.Color = 13290186
        end
        object Sep6: TShape
          Left = 128
          Top = 27
          Width = 1
          Height = 19
          Pen.Color = 13290186
        end
        object ColOutSeparators: TShape
          Left = 264
          Top = 251
          Width = 25
          Height = 18
          Hint = 'Vertical Separators'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColOutSeparatorsMouseDown
        end
        object ColSelLineText: TShape
          Left = 176
          Top = 83
          Width = 25
          Height = 18
          Hint = 'Selected Line Text (Dots)'
          Brush.Color = 15263976
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSelLineTextMouseDown
        end
        object ColSelLineNum: TShape
          Left = 176
          Top = 107
          Width = 25
          Height = 18
          Hint = 'Line numbers of selected line'
          Brush.Color = 15263976
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSelLineNumMouseDown
        end
        object ColSelEnvelope: TShape
          Left = 176
          Top = 131
          Width = 25
          Height = 18
          Hint = 'Selected Line Envelope'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSelEnvelopeMouseDown
        end
        object ColSelNoise: TShape
          Left = 176
          Top = 155
          Width = 25
          Height = 18
          Hint = 'Selected Line Noise'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSelNoiseMouseDown
        end
        object ColSelNote: TShape
          Left = 176
          Top = 179
          Width = 25
          Height = 18
          Hint = 'Selected Line Note'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSelNoteMouseDown
        end
        object ColSelNoteParams: TShape
          Left = 176
          Top = 203
          Width = 25
          Height = 18
          Hint = 'Selected Line Note Params'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSelNoteParamsMouseDown
        end
        object ColSelNoteCommands: TShape
          Left = 176
          Top = 227
          Width = 25
          Height = 18
          Hint = 'Selected Line Special Note Commands'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSelNoteCommandsMouseDown
        end
        object Sep7: TShape
          Left = 370
          Top = 51
          Width = 1
          Height = 251
          Pen.Color = 12171705
        end
        object Sep8: TShape
          Left = 370
          Top = 27
          Width = 1
          Height = 19
          Pen.Color = 13290186
        end
        object LSampleOrnament: TLabel
          Left = 384
          Top = 30
          Width = 86
          Height = 13
          Caption = 'Sample/Ornament'
          Color = 16382455
          ParentColor = False
        end
        object ColSamOrnBackground: TShape
          Left = 384
          Top = 59
          Width = 25
          Height = 18
          Hint = 'Main Background'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamOrnBackgroundMouseDown
        end
        object ColSamOrnText: TShape
          Left = 384
          Top = 83
          Width = 25
          Height = 18
          Hint = 'Text Color'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamOrnTextMouseDown
        end
        object ColSamOrnLineNum: TShape
          Left = 384
          Top = 107
          Width = 25
          Height = 18
          Hint = 'Line Numbers'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamOrnLineNumMouseDown
        end
        object ColSamNoise: TShape
          Left = 384
          Top = 155
          Width = 25
          Height = 18
          Hint = 'Sample Noise'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamNoiseMouseDown
        end
        object ColSamOrnSeparators: TShape
          Left = 384
          Top = 251
          Width = 25
          Height = 18
          Hint = 'Sample/Ornament Separators'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamOrnSeparatorsMouseDown
        end
        object LToneShift: TLabel
          Left = 20
          Top = 277
          Width = 58
          Height = 13
          Caption = 'Tone shift'
          Color = 14737632
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object ColSamTone: TShape
          Left = 384
          Top = 275
          Width = 25
          Height = 18
          Hint = 'Sample Tone Shift'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamToneMouseDown
        end
        object LFullScrBackground: TLabel
          Left = 20
          Top = 310
          Width = 141
          Height = 13
          Caption = 'Full Screen Background:'
          Color = 15461355
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object ColFullScreenBackground: TShape
          Left = 176
          Top = 307
          Width = 25
          Height = 18
          Hint = 'Highlighted Line Background'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColFullScreenBackgroundMouseDown
        end
        object Sep12: TShape
          Left = 166
          Top = 307
          Width = 1
          Height = 19
          Pen.Color = 13290186
        end
        object ColSamOrnSelBackground: TShape
          Left = 416
          Top = 59
          Width = 25
          Height = 18
          Hint = 'Selected Line Background'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamOrnSelBackgroundMouseDown
        end
        object ColSamOrnSelText: TShape
          Left = 416
          Top = 83
          Width = 25
          Height = 18
          Hint = 'Selected Line Text'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamOrnSelTextMouseDown
        end
        object ColSamSelNoise: TShape
          Left = 416
          Top = 155
          Width = 25
          Height = 18
          Hint = 'Selected Line Sample Noise'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamSelNoiseMouseDown
        end
        object ColSamSelTone: TShape
          Left = 416
          Top = 275
          Width = 25
          Height = 18
          Hint = 'Selected Line Sample Tone Shift'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamSelToneMouseDown
        end
        object ColSamOrnSelLineNum: TShape
          Left = 416
          Top = 107
          Width = 25
          Height = 18
          Hint = 'Selected Line Numbers'
          ParentShowHint = False
          Pen.Color = 11447982
          ShowHint = True
          OnMouseDown = ColSamOrnSelLineNumMouseDown
        end
        object Shape2: TShape
          Left = 370
          Top = 307
          Width = 1
          Height = 19
          Pen.Color = 13290186
        end
        object Label2: TLabel
          Left = 272
          Top = 310
          Width = 92
          Height = 13
          Caption = 'Window Theme:'
          Color = 15461355
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Shape3: TShape
          Left = 247
          Top = 307
          Width = 1
          Height = 19
          Pen.Color = 13290186
        end
        object WinColorsBox: TComboBox
          Left = 376
          Top = 306
          Width = 108
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 0
          Text = 'Default'
          OnChange = WinColorsBoxChange
          Items.Strings = (
            'Default'
            'Crystaline'
            'Yellow'
            'Soft'
            'Relief'
            'Flatlines'
            'Light')
        end
      end
      object GroupBox1: TGroupBox
        Left = 240
        Top = 8
        Width = 265
        Height = 145
        Caption = ' Font Settings '
        TabOrder = 2
        object FontBold: TSpeedButton
          Left = 159
          Top = 49
          Width = 45
          Height = 25
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'Bold'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = FontBoldClick
        end
        object Label1: TLabel
          Left = 159
          Top = 94
          Width = 83
          Height = 13
          Hint = 'Number of pattern editor lines'
          Caption = 'Tracker Lines:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object FontsList: TListBox
          Left = 8
          Top = 24
          Width = 145
          Height = 113
          ItemHeight = 13
          TabOrder = 0
          OnClick = FontsListClick
        end
        object FontSize: TEdit
          Left = 159
          Top = 23
          Width = 31
          Height = 21
          TabOrder = 1
          Text = '12'
          OnChange = FontSizeChange
        end
        object FontSizeInt: TUpDown
          Left = 190
          Top = 23
          Width = 14
          Height = 21
          Associate = FontSize
          Min = 12
          Max = 30
          Position = 12
          TabOrder = 2
        end
        object Edit1: TEdit
          Left = 159
          Top = 114
          Width = 31
          Height = 21
          TabOrder = 3
          Text = '31'
          OnChange = Edit1Change
          OnExit = Edit1Exit
        end
        object UpDown1: TUpDown
          Left = 190
          Top = 114
          Width = 14
          Height = 21
          Associate = Edit1
          Min = 11
          Max = 64
          Position = 31
          TabOrder = 4
        end
      end
    end
    object AYEmu: TTabSheet
      Caption = 'Chip emulation'
      ImageIndex = 1
      object Label7: TLabel
        Left = 16
        Top = 485
        Width = 166
        Height = 13
        Caption = 'Some changes will be heared after:'
        Visible = False
      end
      object LBChg: TLabel
        Left = 186
        Top = 485
        Width = 40
        Height = 13
        Caption = '2178 ms'
        Visible = False
      end
      object ChipSel: TRadioGroup
        Left = 128
        Top = 128
        Width = 121
        Height = 65
        Caption = 'Sound chip'
        ItemIndex = 1
        Items.Strings = (
          'AY-3-8910/12'
          'YM2149F')
        TabOrder = 0
        OnClick = ChipSelClick
      end
      object IntSel: TRadioGroup
        Left = 8
        Top = 288
        Width = 241
        Height = 193
        Caption = ' Interrupt frequency '
        ItemIndex = 0
        Items.Strings = (
          '50 Hz (ZX Spectrum)'
          '48.828 Hz (Pentagon 128K)'
          '60 Hz (Atari ST)'
          '100 Hz (Twice per INT)'
          '200 Hz (Atari ST)'
          '48 Hz (NonFractional BPM)'
          'Manual (mHz)')
        TabOrder = 1
        OnClick = IntSelClick
      end
      object Opt: TRadioGroup
        Left = 8
        Top = 200
        Width = 113
        Height = 81
        Caption = ' Optimization '
        ItemIndex = 0
        Items.Strings = (
          'for quality'
          'for perfomance')
        TabOrder = 2
        OnClick = OptClick
      end
      object ChFreq: TRadioGroup
        Left = 256
        Top = 8
        Width = 249
        Height = 473
        Caption = ' Chip frequency '
        ItemIndex = 0
        Items.Strings = (
          '1,7734 MHz (ZX Spectrum)'
          '1,75 MHz (Pentagon 128K)'
          '2 MHz (Atari ST)'
          '1 MHz (Amstard CPC)'
          '3,5 MHz'
          '1520640 MHz (Natural C/Am for 4th table)'
          '1611062 MHz (Natural C#/A#m for 4th table)'
          '1706861 MHz (Natural D/Bm for 4th table)'
          '1808356 MHz (Natural D#/Cm for 4th table)'
          '1915886 MHz (Natural E/C#m for 4th table)'
          '2029811 MHz (Natural F/Dm for 4th table)'
          '2150510 MHz (Natural F#/D#m for 4th table)'
          '2278386 MHz (Natural G/Em for 4th table)'
          '2413866 MHz (Natural G#/Fm for 4th table)'
          '2557401 MHz (Natural A/F#m for 4th table)'
          '2709472 MHz (Natural A#/Gm for 4th table)'
          '2870586 MHz (Natural B/G#m for 4th table)'
          '3041280 MHz (Natural C/Am for 4th table)'
          'Manual (Hz)')
        TabOrder = 3
        OnClick = ChFreqClick
      end
      object FiltersGroup: TGroupBox
        Left = 128
        Top = 200
        Width = 121
        Height = 81
        Caption = ' Downsampling '
        TabOrder = 4
        object Label9: TLabel
          Left = 7
          Top = 61
          Width = 12
          Height = 13
          Caption = 'Lo'
        end
        object Label10: TLabel
          Left = 100
          Top = 61
          Width = 10
          Height = 13
          Caption = 'Hi'
        end
        object FiltChk: TCheckBox
          Left = 10
          Top = 20
          Width = 65
          Height = 17
          Caption = 'FIR-filter'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = FiltChkClick
        end
        object FiltNK: TTrackBar
          Left = 6
          Top = 43
          Width = 108
          Height = 17
          Max = 9
          Min = 4
          PageSize = 1
          Position = 5
          TabOrder = 1
          ThumbLength = 10
          OnChange = FiltNKChange
        end
      end
      object EdChipFrq: TEdit
        Left = 346
        Top = 448
        Width = 73
        Height = 17
        AutoSize = False
        TabOrder = 5
        OnChange = EdChipFrqExit
      end
      object EdIntFrq: TEdit
        Left = 105
        Top = 456
        Width = 65
        Height = 17
        AutoSize = False
        TabOrder = 6
        OnChange = EdIntFrqExit
      end
      object PanoramBox: TGroupBox
        Left = 128
        Top = 8
        Width = 121
        Height = 113
        Caption = ' Panning  '
        TabOrder = 7
        object APanLabel: TLabel
          Left = 11
          Top = 25
          Width = 7
          Height = 13
          Caption = 'A'
        end
        object BPanLabel: TLabel
          Left = 11
          Top = 54
          Width = 7
          Height = 13
          Caption = 'B'
        end
        object CPanLabel: TLabel
          Left = 11
          Top = 83
          Width = 7
          Height = 13
          Caption = 'C'
        end
        object APan: TTrackBar
          Left = 22
          Top = 24
          Width = 91
          Height = 17
          LineSize = 3
          Max = 255
          TabOrder = 0
          ThumbLength = 10
          TickStyle = tsManual
          OnChange = APanChange
        end
        object BPan: TTrackBar
          Left = 22
          Top = 53
          Width = 91
          Height = 17
          Max = 255
          TabOrder = 1
          ThumbLength = 10
          TickStyle = tsManual
          OnChange = BPanChange
        end
        object CPan: TTrackBar
          Left = 22
          Top = 82
          Width = 91
          Height = 17
          Max = 255
          TabOrder = 2
          ThumbLength = 10
          TickStyle = tsManual
          OnChange = CPanChange
        end
      end
      object ChanVisAlloc: TRadioGroup
        Left = 8
        Top = 8
        Width = 113
        Height = 185
        Caption = ' Channels allocation '
        ItemIndex = 0
        Items.Strings = (
          'Mono'
          'ABC'
          'ACB'
          'BAC'
          'BCA'
          'CAB'
          'CBA')
        TabOrder = 8
        OnClick = ChanVisAllocClick
      end
    end
    object WOAPITAB: TTabSheet
      Caption = 'Audio'
      ImageIndex = 4
      object SpeedButton1: TSpeedButton
        Left = 448
        Top = 424
        Width = 55
        Height = 22
        Action = MainForm.Stop
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          1800000000000003000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF000000000000000000000000000000000000000000FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000
          0000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF000000000000000000000000000000000000000000FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000
          0000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF000000000000000000000000000000000000000000FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000
          0000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF000000000000000000000000000000000000000000FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000
          0000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      end
      object grp1: TGroupBox
        Left = 264
        Top = 184
        Width = 241
        Height = 137
        Caption = 'MIDI Keyboard  '
        TabOrder = 5
        object midibtn1: TButton
          Left = 136
          Top = 64
          Width = 89
          Height = 25
          Caption = 'Next Device >'
          TabOrder = 0
          OnClick = midibtn1Click
        end
        object midibtn2: TButton
          Left = 16
          Top = 64
          Width = 97
          Height = 25
          Caption = '< Previous Device'
          TabOrder = 1
          OnClick = midibtn2Click
        end
        object cmbInput: TComboBox
          Left = 16
          Top = 28
          Width = 209
          Height = 21
          ItemHeight = 0
          TabOrder = 2
          Text = 'cmbInput'
        end
        object midibtn3: TButton
          Left = 16
          Top = 96
          Width = 209
          Height = 25
          Caption = 'Stop MIDI'
          TabOrder = 3
          OnClick = midibtn3Click
        end
      end
      object SR: TRadioGroup
        Left = 8
        Top = 8
        Width = 129
        Height = 161
        Caption = 'Sample Rate  '
        ItemIndex = 2
        Items.Strings = (
          '11025 Hz'
          '22050 Hz'
          '44100 Hz'
          '48000 Hz'
          '88200 Hz'
          '96000 Hz')
        TabOrder = 0
        OnClick = SRClick
      end
      object BR: TRadioGroup
        Left = 152
        Top = 8
        Width = 97
        Height = 73
        Caption = 'Bit Rate  '
        ItemIndex = 1
        Items.Strings = (
          '8 bit'
          '16 bit')
        TabOrder = 1
        OnClick = BRClick
      end
      object NCh: TRadioGroup
        Left = 152
        Top = 88
        Width = 97
        Height = 81
        Caption = 'Channels'
        ItemIndex = 1
        Items.Strings = (
          'Mono'
          'Stereo')
        TabOrder = 2
        OnClick = NChClick
      end
      object Buff: TGroupBox
        Left = 264
        Top = 8
        Width = 241
        Height = 161
        Caption = 'Buffers'
        TabOrder = 3
        object Label6: TLabel
          Left = 16
          Top = 88
          Width = 87
          Height = 13
          Caption = 'Number of buffers:'
        end
        object LbLen: TLabel
          Left = 88
          Top = 24
          Width = 34
          Height = 13
          Caption = '726 ms'
        end
        object LbNum: TLabel
          Left = 107
          Top = 88
          Width = 6
          Height = 13
          Caption = '3'
        end
        object Label4: TLabel
          Left = 16
          Top = 137
          Width = 63
          Height = 13
          Caption = 'Total Length:'
        end
        object LBTot: TLabel
          Left = 85
          Top = 137
          Width = 40
          Height = 13
          Caption = '2178 ms'
        end
        object Label5: TLabel
          Left = 16
          Top = 24
          Width = 67
          Height = 13
          Caption = 'Buffer Length:'
        end
        object TrackBar1: TTrackBar
          Left = 8
          Top = 40
          Width = 225
          Height = 33
          Hint = 'Length of one buffer'
          Max = 2000
          Min = 5
          PageSize = 1
          Frequency = 100
          Position = 726
          TabOrder = 0
          OnChange = TrackBar1Change
        end
        object TrackBar2: TTrackBar
          Left = 8
          Top = 103
          Width = 225
          Height = 33
          Hint = 'Number of buffers'
          Min = 2
          PageSize = 1
          Position = 3
          TabOrder = 1
          OnChange = TrackBar2Change
        end
      end
      object SelDev: TGroupBox
        Left = 8
        Top = 184
        Width = 241
        Height = 137
        Caption = 'Wave Out Device  '
        TabOrder = 4
        object Edit3: TEdit
          Left = 16
          Top = 28
          Width = 209
          Height = 21
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 0
          Text = 'Wave mapper'
        end
        object Button4: TButton
          Left = 16
          Top = 96
          Width = 209
          Height = 25
          Caption = 'Get full list'
          TabOrder = 1
          OnClick = Button4Click
        end
        object ComboBox1: TComboBox
          Left = 16
          Top = 64
          Width = 209
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 2
          Visible = False
          OnChange = ComboBox1Change
        end
      end
    end
    object HotKeys: TTabSheet
      Caption = 'HotKeys'
      ImageIndex = 6
      object GroupBox2: TGroupBox
        Left = 8
        Top = 8
        Width = 497
        Height = 481
        TabOrder = 0
        object HotKeyList: TListView
          Left = 8
          Top = 14
          Width = 481
          Height = 459
          BiDiMode = bdLeftToRight
          Columns = <
            item
              AutoSize = True
              Caption = 'Name'
              MaxWidth = 335
              MinWidth = 335
            end
            item
              AutoSize = True
              Caption = 'HotKey'
              MaxWidth = 142
              MinWidth = 142
            end>
          ColumnClick = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          GridLines = True
          IconOptions.Arrangement = iaLeft
          IconOptions.WrapText = False
          ReadOnly = True
          RowSelect = True
          ParentBiDiMode = False
          ParentFont = False
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
          TabStop = False
          ViewStyle = vsReport
          OnKeyDown = HotKeyListKeyDown
          OnKeyPress = HotKeyListKeyPress
        end
      end
    end
    object OpMod: TTabSheet
      Caption = 'Compatibility'
      ImageIndex = 3
      object SaveHead: TRadioGroup
        Left = 8
        Top = 160
        Width = 497
        Height = 153
        Caption = 'Save with header'
        ItemIndex = 2
        Items.Strings = (
          '"Vortex Tracker II 2.0 module:" where possible'
          '"ProTracker 3.x compilation of" always'
          'Try detect')
        TabOrder = 1
        OnClick = SaveHeadClick
      end
      object RadioGroup1: TRadioGroup
        Left = 8
        Top = 328
        Width = 497
        Height = 161
        Caption = 'Features level'
        ItemIndex = 3
        Items.Strings = (
          'Pro Tracker 3.5'
          'Vortex Tracker II (PT 3.6)'
          'Pro Tracker 3.7'
          'Try detect')
        TabOrder = 0
        OnClick = RadioGroup1Click
      end
      object FileAssocBox: TGroupBox
        Left = 8
        Top = 8
        Width = 497
        Height = 137
        Caption = ' File Associations '
        TabOrder = 2
        object VT2Assoc: TCheckBox
          Left = 16
          Top = 32
          Width = 169
          Height = 17
          Caption = ' *.vt2 - VortexTracker module'
          TabOrder = 0
          OnClick = VT2AssocClick
        end
        object VTTAssoc: TCheckBox
          Left = 16
          Top = 97
          Width = 129
          Height = 17
          Caption = ' *.vtt - Color Theme'
          TabOrder = 1
          OnClick = VTTAssocClick
        end
        object PT3Assoc: TCheckBox
          Left = 16
          Top = 66
          Width = 169
          Height = 17
          Caption = ' *.pt3 - ProTracker 3 module'
          TabOrder = 2
          OnClick = PT3AssocClick
        end
      end
    end
  end
  object Button1: TButton
    Left = 336
    Top = 536
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 432
    Top = 536
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object SaveThemeDialog: TSaveDialog
    DefaultExt = 'vtt'
    Filter = 'Vortext Tracker Theme (*.vtt)|*.vtt'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofNoChangeDir, ofCreatePrompt, ofNoReadOnlyReturn, ofEnableSizing]
    Left = 16
    Top = 536
  end
  object LoadThemeDialog: TOpenDialog
    DefaultExt = 'vtt'
    Filter = 'Vortext Tracker Theme (*.vtt)|*.vtt'
    Options = [ofHideReadOnly, ofNoChangeDir, ofFileMustExist, ofNoLongNames, ofEnableSizing]
    Left = 48
    Top = 536
  end
  object TemplateDialog: TOpenDialog
    DefaultExt = 'vt2'
    Filter = 
      'VortexTracker 2.0 module (*.vt2)|*.vt2|VortexTracker 1.0 module ' +
      '(*.txt)|*.txt'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 80
    Top = 536
  end
end
