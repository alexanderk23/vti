object MDIChild: TMDIChild
  Left = 785
  Top = 154
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'MDI Child'
  ClientHeight = 562
  ClientWidth = 538
  Color = clBtnFace
  ParentFont = True
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDblClick = FormDblClick
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnPaint = FormPaint
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object TopBackgroundBox: TShape
    Left = 0
    Top = 600
    Width = 769
    Height = 9
    Brush.Color = clBtnFace
    Pen.Color = clBtnFace
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 538
    Height = 585
    ActivePage = PatternsSheet
    BiDiMode = bdLeftToRight
    Constraints.MinWidth = 538
    Images = MainForm.ImageList1
    ParentBiDiMode = False
    TabHeight = 19
    TabOrder = 0
    object PatternsSheet: TTabSheet
      Caption = 'Patterns'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpVariable
      Font.Style = []
      ImageIndex = 29
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      object StringGridTop: TShape
        Left = 0
        Top = 84
        Width = 609
        Height = 1
        Pen.Color = clGrayText
      end
      object AutoHLBox: TGroupBox
        Left = 0
        Top = 126
        Width = 97
        Height = 33
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 8
        object AutoHL: TSpeedButton
          Left = 4
          Top = 9
          Width = 45
          Height = 20
          Hint = 'Highlight step'
          AllowAllUp = True
          GroupIndex = 1
          Down = True
          Caption = 'Auto'
          Flat = True
          OnClick = AutoHLCheckClick
        end
        object Edit17: TEdit
          Left = 48
          Top = 9
          Width = 25
          Height = 20
          Hint = 'Highlight step'
          AutoSize = False
          TabOrder = 0
          Text = '4'
          OnExit = Edit17Exit
          OnKeyPress = Edit17KeyPress
        end
        object UpDown15: TUpDown
          Left = 73
          Top = 9
          Width = 16
          Height = 20
          Hint = 'Highlight step'
          Associate = Edit17
          Position = 4
          TabOrder = 1
          OnChangingEx = UpDown15ChangingEx
          OnClick = UpDown15Click
        end
      end
      object Channel1Box: TGroupBox
        Left = 96
        Top = 126
        Width = 142
        Height = 33
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 9
        object SpeedButton1: TSpeedButton
          Left = 5
          Top = 9
          Width = 50
          Height = 20
          Hint = 'Mute Channel'
          AllowAllUp = True
          GroupIndex = 10
          Caption = 'Chan A'
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton1Click
        end
        object SpeedButton2: TSpeedButton
          Left = 76
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Tone'
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'T'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton2Click
        end
        object SpeedButton3: TSpeedButton
          Left = 96
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Noise'
          AllowAllUp = True
          GroupIndex = 2
          Caption = 'N'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton3Click
        end
        object SpeedButton4: TSpeedButton
          Left = 117
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Envelope'
          AllowAllUp = True
          GroupIndex = 3
          Caption = 'E'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton4Click
        end
        object SpeedButton13: TSpeedButton
          Left = 55
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Solo Channel'
          AllowAllUp = True
          GroupIndex = 13
          Caption = 'S'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton13Click
        end
      end
      object PatEmptyBox: TGroupBox
        Left = 0
        Top = -2
        Width = 1127
        Height = 58
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 7
      end
      object Channel2Box: TGroupBox
        Left = 240
        Top = 126
        Width = 145
        Height = 33
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 10
        object SpeedButton5: TSpeedButton
          Left = 5
          Top = 9
          Width = 50
          Height = 20
          Hint = 'Mute Channel'
          AllowAllUp = True
          GroupIndex = 11
          Caption = 'Chan B'
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton5Click
        end
        object SpeedButton6: TSpeedButton
          Left = 77
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Tone'
          AllowAllUp = True
          GroupIndex = 4
          Caption = 'T'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton6Click
        end
        object SpeedButton7: TSpeedButton
          Left = 97
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Noise'
          AllowAllUp = True
          GroupIndex = 5
          Caption = 'N'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton7Click
        end
        object SpeedButton8: TSpeedButton
          Left = 117
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Envelope'
          AllowAllUp = True
          GroupIndex = 6
          Caption = 'E'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton8Click
        end
        object SpeedButton14: TSpeedButton
          Left = 57
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Solo Channel'
          AllowAllUp = True
          GroupIndex = 14
          Caption = 'S'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton14Click
        end
      end
      object Channel3Box: TGroupBox
        Left = 384
        Top = 126
        Width = 145
        Height = 33
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 11
        object SpeedButton9: TSpeedButton
          Left = 7
          Top = 9
          Width = 50
          Height = 20
          Hint = 'Mute Channel'
          AllowAllUp = True
          GroupIndex = 12
          Caption = 'Chan C'
          Flat = True
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton9Click
        end
        object SpeedButton10: TSpeedButton
          Left = 79
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Tone'
          AllowAllUp = True
          GroupIndex = 7
          Caption = 'T'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton10Click
        end
        object SpeedButton11: TSpeedButton
          Left = 99
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Noise'
          AllowAllUp = True
          GroupIndex = 8
          Caption = 'N'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton11Click
        end
        object SpeedButton12: TSpeedButton
          Left = 121
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Mute Envelope'
          AllowAllUp = True
          GroupIndex = 9
          Caption = 'E'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton12Click
        end
        object SpeedButton15: TSpeedButton
          Left = 59
          Top = 9
          Width = 20
          Height = 20
          Hint = 'Solo Channel'
          AllowAllUp = True
          GroupIndex = 15
          Caption = 'S'
          Flat = True
          Margin = 4
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton15Click
        end
      end
      object TrackInfoBox: TGroupBox
        Left = -2
        Top = 49
        Width = 800
        Height = 34
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 5
        object Label6: TLabel
          Left = 239
          Top = 13
          Width = 11
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'by'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentBiDiMode = False
          ParentColor = False
          ParentFont = False
        end
        object Edit3: TEdit
          Left = 2
          Top = 9
          Width = 231
          Height = 21
          Hint = 'Song title'
          BiDiMode = bdLeftToRight
          MaxLength = 32
          ParentBiDiMode = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnChange = Edit3Change
          OnKeyPress = Edit3KeyPress
        end
        object Edit4: TEdit
          Left = 256
          Top = 9
          Width = 233
          Height = 21
          Hint = 'Author name'
          BiDiMode = bdLeftToRight
          MaxLength = 32
          ParentBiDiMode = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnChange = Edit4Change
          OnKeyPress = Edit4KeyPress
        end
      end
      object PatOptions: TGroupBox
        Left = -2
        Top = -2
        Width = 181
        Height = 58
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        Ctl3D = True
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        ParentCtl3D = False
        TabOrder = 0
        object Label2: TLabel
          Left = 71
          Top = 15
          Width = 34
          Height = 13
          Hint = 'Current pattern (press [+]/[-] on NUMpad to change)'
          Caption = 'Pattern'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label5: TLabel
          Left = 126
          Top = 15
          Width = 33
          Height = 13
          Hint = 'Pattern length'
          Caption = 'Length'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object SpeedButton26: TSpeedButton
          Left = 2
          Top = 10
          Width = 55
          Height = 21
          Hint = 'Load pattern'
          BiDiMode = bdLeftToRight
          Caption = 'Load'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -4
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ParentBiDiMode = False
          ShowHint = True
          OnClick = SpeedButton26Click
        end
        object SpeedButton27: TSpeedButton
          Left = 2
          Top = 32
          Width = 55
          Height = 21
          Hint = 'Save pattern'
          Caption = 'Save'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = 8
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton27Click
        end
        object PatternNumUpDown: TUpDown
          Left = 96
          Top = 32
          Width = 15
          Height = 20
          Hint = 'Current pattern (press [+]/[-] on NUMpad to change)'
          Associate = PatternNumEdit
          Max = 84
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnChangingEx = PatternNumUpDownChangingEx
        end
        object PatternNumEdit: TEdit
          Left = 71
          Top = 32
          Width = 25
          Height = 20
          Hint = 'Current pattern (press [+]/[-] on NUMpad to change)'
          AutoSize = False
          BevelEdges = []
          BevelInner = bvNone
          BevelOuter = bvNone
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = '0'
          OnChange = PatternNumEditChange
          OnExit = PatternNumEditExit
          OnKeyPress = PatternNumEditKeyPress
        end
        object PatternLenEdit: TEdit
          Left = 126
          Top = 32
          Width = 25
          Height = 20
          Hint = 'Pattern length'
          AutoSize = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Text = '64'
          OnExit = PatternLenEditExit
          OnKeyDown = PatternLenEditKeyDown
          OnKeyPress = PatternLenEditKeyPress
        end
        object PatternLenUpDown: TUpDown
          Left = 151
          Top = 32
          Width = 15
          Height = 20
          Hint = 'Pattern length'
          Min = 1
          ParentShowHint = False
          Position = 64
          ShowHint = True
          TabOrder = 3
          OnChangingEx = PatternLenUpDownChangingEx
        end
      end
      object SpeedBox: TGroupBox
        Left = 177
        Top = -2
        Width = 86
        Height = 58
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        Ctl3D = True
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        ParentCtl3D = False
        TabOrder = 3
        object Label3: TLabel
          Left = 14
          Top = 15
          Width = 59
          Height = 13
          Hint = 'Initial speed'
          Caption = 'Speed/BPM'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object SpeedBpmEdit: TEdit
          Left = 14
          Top = 32
          Width = 43
          Height = 20
          Hint = 'Initial speed'
          AutoSize = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          Text = '3'
          OnKeyPress = SpeedBpmEditKeyPress
        end
        object SpeedBpmUpDown: TUpDown
          Left = 57
          Top = 32
          Width = 15
          Height = 20
          Hint = 'Initial speed'
          Min = 1
          Max = 255
          ParentShowHint = False
          Position = 3
          ShowHint = True
          TabOrder = 1
          OnChangingEx = SpeedBpmUpDownChangingEx
        end
      end
      object OctaveBox: TGroupBox
        Left = 261
        Top = -2
        Width = 68
        Height = 58
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 4
        object Label1: TLabel
          Left = 14
          Top = 15
          Width = 35
          Height = 13
          Hint = 'Octave number (Numpad 1-8 when cursor in a note cell)'
          Caption = 'Octave'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object OctaveEdit: TEdit
          Left = 14
          Top = 32
          Width = 25
          Height = 20
          Hint = 'Octave number (Numpad 1-8 when cursor in a note cell)'
          AutoSize = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '3'
          OnExit = OctaveEditExit
          OnKeyPress = OctaveEditKeyPress
        end
        object OctaveUpDown: TUpDown
          Left = 39
          Top = 32
          Width = 15
          Height = 20
          Hint = 'Octave number (Numpad 1-8 when cursor in a note cell)'
          Associate = OctaveEdit
          Min = 1
          Max = 8
          ParentShowHint = False
          Position = 3
          ShowHint = True
          TabOrder = 1
        end
      end
      object AutoStepBox: TGroupBox
        Left = 327
        Top = -2
        Width = 94
        Height = 58
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 1
        object AutoStepBtn: TSpeedButton
          Left = 14
          Top = 11
          Width = 66
          Height = 20
          Hint = 'Toggle autostep (Ctrl+R or Space when editing tracks)'
          AllowAllUp = True
          GroupIndex = 11
          Caption = 'Auto Step'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          Layout = blGlyphRight
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = AutoStepBtnClick
        end
        object AutoStepUpDown: TUpDown
          Left = 65
          Top = 32
          Width = 15
          Height = 20
          Hint = 'Edit step (spacing)'
          Associate = AutoStepEdit
          Min = -64
          Max = 64
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object AutoStepEdit: TEdit
          Left = 14
          Top = 32
          Width = 51
          Height = 20
          Hint = 'Edit step (spacing)'
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = '0'
          OnExit = AutoStepEditExit
          OnKeyPress = AutoStepEditKeyPress
        end
      end
      object InterfaceOpts: TGroupBox
        Left = -10
        Top = 268
        Width = 771
        Height = 33
        Color = clBtnFace
        ParentColor = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        object EnvelopeAsNote: TCheckBox
          Left = 12
          Top = 11
          Width = 117
          Height = 17
          Hint = 'Envelope As Note (press [/] on Numpad to change)'
          Caption = 'Envelope as Note'
          Ctl3D = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EnvelopeAsNoteClick
          OnMouseUp = EnvelopeAsNoteMouseUp
        end
        object DuplicateNoteParams: TCheckBox
          Left = 220
          Top = 11
          Width = 125
          Height = 17
          Hint = 'Use sample, envelope, ornament and value of last note'
          Caption = 'Use last note params'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnMouseDown = DuplicateNoteParamsMouseDown
        end
        object BetweenPatterns: TCheckBox
          Left = 398
          Top = 11
          Width = 133
          Height = 17
          Hint = 'Move continuously between patterns while editing'
          Caption = 'Move between patterns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Pitch = fpVariable
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnMouseDown = BetweenPatternsMouseDown
        end
      end
      object AutoEnvBox: TGroupBox
        Left = 419
        Top = -2
        Width = 94
        Height = 58
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        TabOrder = 12
        object AutoEnvBtn: TSpeedButton
          Left = 14
          Top = 11
          Width = 66
          Height = 20
          Hint = 'Toggle autoenvelope (Ctrl+E or Numpad 0 when editing tracks)'
          AllowAllUp = True
          GroupIndex = 10
          Caption = 'Auto Env'
          Layout = blGlyphRight
          ParentShowHint = False
          ShowHint = True
          OnClick = AutoEnvBtnClick
        end
        object SpeedButton16: TSpeedButton
          Left = 58
          Top = 32
          Width = 22
          Height = 20
          Hint = 'Tone frequency'
          Caption = '1'
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton16Click
        end
        object SpeedButton17: TSpeedButton
          Left = 36
          Top = 32
          Width = 22
          Height = 20
          Hint = 'Toggle standard combinations (Ctrl+Alt+E)'
          Caption = ':'
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton17Click
        end
        object SpeedButton18: TSpeedButton
          Left = 14
          Top = 32
          Width = 22
          Height = 20
          Hint = 'Envelope frequency'
          Caption = '1'
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton18Click
        end
      end
      object StringGrid1: TStringGrid
        Left = 0
        Top = 86
        Width = 530
        Height = 43
        Cursor = crArrow
        Hint = 'Position list'
        BiDiMode = bdLeftToRight
        BorderStyle = bsNone
        Color = clWhite
        ColCount = 256
        Constraints.MinHeight = 42
        Ctl3D = True
        DefaultColWidth = 42
        DefaultRowHeight = 32
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Pitch = fpFixed
        Font.Style = [fsBold]
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goThumbTracking]
        ParentBiDiMode = False
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = MainForm.PopupMenu1
        ScrollBars = ssHorizontal
        TabOrder = 2
        OnDragDrop = StringGrid1DragDrop
        OnDragOver = StringGrid1DragOver
        OnDrawCell = StringGrid1DrawCell
        OnEndDrag = StringGrid1EndDrag
        OnKeyDown = StringGrid1KeyDown
        OnKeyPress = StringGrid1KeyPress
        OnKeyUp = StringGrid1KeyUp
        OnMouseDown = StringGrid1MouseDown
        OnMouseUp = StringGrid1MouseUp
        OnSelectCell = StringGrid1SelectCell
      end
    end
    object SamplesSheet: TTabSheet
      Caption = 'Samples'
      ImageIndex = 31
      OnEnter = SamplesSheetEnter
      object GroupBox2: TGroupBox
        Left = 335
        Top = 186
        Width = 183
        Height = 273
        TabOrder = 0
        object Label13: TLabel
          Left = 96
          Top = 21
          Width = 3
          Height = 13
        end
        object Label12: TLabel
          Left = 12
          Top = 20
          Width = 81
          Height = 13
          Caption = 'Sample in tracks:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
      object SampleEditBox: TGroupBox
        Left = 0
        Top = 42
        Width = 337
        Height = 431
        TabOrder = 3
      end
      object SampleBox: TGroupBox
        Left = 335
        Top = -2
        Width = 183
        Height = 195
        TabOrder = 1
        object Label9: TLabel
          Left = 16
          Top = 18
          Width = 35
          Height = 13
          Hint = 'Current sample'
          Caption = 'Sample'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label11: TLabel
          Left = 16
          Top = 70
          Width = 24
          Height = 13
          Hint = 'Sample loop'
          Caption = 'Loop'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label10: TLabel
          Left = 85
          Top = 18
          Width = 33
          Height = 13
          Hint = 'Sample length'
          Caption = 'Length'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
        end
        object Label4: TLabel
          Left = 84
          Top = 70
          Width = 39
          Height = 13
          Hint = 'Copy current sample to destination'
          Caption = 'Copy to:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object CopySamBut: TSpeedButton
          Left = 85
          Top = 115
          Width = 48
          Height = 21
          Hint = 'Copy current sample to selected one'
          Caption = 'Copy'
          Margin = 9
          ParentShowHint = False
          ShowHint = True
          OnClick = CopySamButClick
        end
        object SampleNumEdit: TEdit
          Left = 16
          Top = 37
          Width = 33
          Height = 21
          Hint = 'Current sample'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '1'
          OnChange = SampleNumEditChange
          OnExit = SampleNumEditExit
          OnKeyPress = SampleNumEditKeyPress
        end
        object SampleNumUpDown: TUpDown
          Left = 49
          Top = 37
          Width = 15
          Height = 21
          Hint = 'Current sample'
          Associate = SampleNumEdit
          Min = 1
          Max = 31
          ParentShowHint = False
          Position = 1
          ShowHint = True
          TabOrder = 1
          OnChangingEx = SampleNumUpDownChangingEx
        end
        object SampleLoopEdit: TEdit
          Left = 16
          Top = 89
          Width = 33
          Height = 21
          Hint = 'Sample loop'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Text = '0'
          OnExit = SampleLoopEditExit
          OnKeyPress = SampleLoopEditKeyPress
        end
        object SampleLoopUpDown: TUpDown
          Left = 49
          Top = 89
          Width = 15
          Height = 21
          Hint = 'Sample loop'
          Max = 63
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnChangingEx = SampleLoopUpDownChangingEx
        end
        object SampleLenEdit: TEdit
          Left = 85
          Top = 37
          Width = 33
          Height = 21
          Hint = 'Sample length'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Text = '1'
          OnExit = SampleLenEditExit
          OnKeyPress = SampleLenEditKeyPress
        end
        object SampleLenUpDown: TUpDown
          Left = 118
          Top = 37
          Width = 15
          Height = 21
          Hint = 'Sample length'
          Min = 1
          Max = 64
          ParentShowHint = False
          Position = 1
          ShowHint = True
          TabOrder = 5
          OnChangingEx = SampleLenUpDownChangingEx
        end
        object SampleCopyToEdit: TEdit
          Left = 85
          Top = 89
          Width = 33
          Height = 21
          Hint = 'Destination sample'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          Text = '1'
          OnContextPopup = SampleCopyToEditContextPopup
          OnExit = SampleCopyToEditExit
          OnKeyPress = SampleCopyToEditKeyPress
          OnMouseDown = SampleCopyToEditMouseDown
        end
        object SampleCopyToUpDown: TUpDown
          Left = 118
          Top = 89
          Width = 15
          Height = 21
          Hint = 'Destination sample'
          Associate = SampleCopyToEdit
          Min = 1
          Max = 31
          ParentShowHint = False
          Position = 1
          ShowHint = True
          TabOrder = 7
          OnChangingEx = SampleCopyToUpDownChangingEx
        end
        object UnloopBtn: TButton
          Left = 16
          Top = 115
          Width = 48
          Height = 21
          Caption = 'Unloop'
          TabOrder = 8
          OnClick = UnloopBtnClick
        end
        object ClearSample: TButton
          Left = 16
          Top = 144
          Width = 49
          Height = 21
          Hint = 'Clear Sample'
          Caption = 'Clear'
          TabOrder = 9
          OnClick = ClearSampleClick
        end
      end
      object SamplesTestFieldBox: TGroupBox
        Left = 0
        Top = -2
        Width = 337
        Height = 51
        TabOrder = 2
        object SpeedButton25: TSpeedButton
          Left = 229
          Top = 14
          Width = 49
          Height = 27
          Hint = 'Load sample'
          Caption = 'Load'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Margin = 9
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton25Click
        end
        object SpeedButton24: TSpeedButton
          Left = 282
          Top = 14
          Width = 49
          Height = 27
          Hint = 'Save sample'
          Caption = 'Save'
          Margin = 9
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton24Click
        end
      end
    end
    object OrnamentsSheet: TTabSheet
      Caption = 'Ornaments'
      ImageIndex = 30
      OnEnter = OrnamentsSheetEnter
      object SpeedButton21: TSpeedButton
        Left = 456
        Top = 376
        Width = 41
        Height = 22
        Hint = 'External ornament generator plug-in'
        Caption = 'OrGen'
        Visible = False
        OnClick = SpeedButton21Click
      end
      object GroupBox4: TGroupBox
        Left = 367
        Top = 140
        Width = 154
        Height = 175
        TabOrder = 0
        object Label32: TLabel
          Left = 16
          Top = 20
          Width = 92
          Height = 13
          Caption = 'Ornament in tracks:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label33: TLabel
          Left = 111
          Top = 21
          Width = 3
          Height = 13
        end
      end
      object OrnamentEditBox: TGroupBox
        Left = 0
        Top = 42
        Width = 369
        Height = 281
        TabOrder = 2
      end
      object OrnamentsTestFieldBox: TGroupBox
        Left = 0
        Top = -2
        Width = 369
        Height = 51
        TabOrder = 1
        object SpeedButton19: TSpeedButton
          Left = 245
          Top = 14
          Width = 49
          Height = 27
          Hint = 'Load ornament'
          Caption = 'Load'
          Margin = 9
          OnClick = SpeedButton19Click
        end
        object SpeedButton20: TSpeedButton
          Left = 306
          Top = 14
          Width = 49
          Height = 27
          Hint = 'Save ornament'
          Caption = 'Save'
          Margin = 9
          OnClick = SpeedButton20Click
        end
      end
      object OrnamentBox: TGroupBox
        Left = 367
        Top = -2
        Width = 154
        Height = 149
        TabOrder = 3
        object Label31: TLabel
          Left = 16
          Top = 18
          Width = 46
          Height = 13
          Caption = 'Ornament'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label30: TLabel
          Left = 16
          Top = 70
          Width = 24
          Height = 13
          Caption = 'Loop'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label29: TLabel
          Left = 85
          Top = 18
          Width = 33
          Height = 13
          Caption = 'Length'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 85
          Top = 70
          Width = 39
          Height = 13
          Caption = 'Copy to:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMenuText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object CopyOrnBut: TSpeedButton
          Left = 85
          Top = 115
          Width = 49
          Height = 21
          Hint = 'Copy current ornament to selected one'
          Caption = 'Copy'
          Margin = 9
          ParentShowHint = False
          ShowHint = True
          OnClick = CopyOrnButClick
        end
        object OrnamentNumEdit: TEdit
          Left = 16
          Top = 37
          Width = 33
          Height = 21
          Hint = 'Current ornament'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '1'
          OnChange = OrnamentNumEditChange
          OnExit = OrnamentNumEditExit
          OnKeyPress = OrnamentNumEditKeyPress
        end
        object OrnamentNumUpDown: TUpDown
          Left = 49
          Top = 37
          Width = 15
          Height = 21
          Hint = 'Current ornament'
          Associate = OrnamentNumEdit
          Min = 1
          Max = 15
          ParentShowHint = False
          Position = 1
          ShowHint = True
          TabOrder = 1
          OnChangingEx = OrnamentNumUpDownChangingEx
        end
        object OrnamentLoopEdit: TEdit
          Left = 16
          Top = 89
          Width = 33
          Height = 21
          Hint = 'Ornament loop'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Text = '0'
          OnExit = OrnamentLoopEditExit
          OnKeyPress = OrnamentLoopEditKeyPress
        end
        object OrnamentLoopUpDown: TUpDown
          Left = 49
          Top = 89
          Width = 15
          Height = 21
          Hint = 'Ornament loop'
          Max = 63
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnChangingEx = OrnamentLoopUpDownChangingEx
        end
        object OrnamentLenEdit: TEdit
          Left = 85
          Top = 37
          Width = 33
          Height = 21
          Hint = 'Ornament length'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Text = '1'
          OnExit = OrnamentLenEditExit
          OnKeyPress = OrnamentLenEditKeyPress
        end
        object OrnamentLenUpDown: TUpDown
          Left = 118
          Top = 37
          Width = 15
          Height = 21
          Hint = 'Ornament length'
          Min = 1
          Max = 64
          ParentShowHint = False
          Position = 1
          ShowHint = True
          TabOrder = 5
          OnChangingEx = OrnamentLenUpDownChangingEx
        end
        object OrnamentCopyToEdit: TEdit
          Left = 85
          Top = 89
          Width = 33
          Height = 21
          Hint = 'Distination ornament'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          Text = '1'
          OnExit = OrnamentCopyToEditExit
          OnKeyPress = OrnamentCopyToEditKeyPress
        end
        object OrnamentCopyToUpDown: TUpDown
          Left = 118
          Top = 89
          Width = 15
          Height = 21
          Hint = 'Distination ornament'
          Associate = OrnamentCopyToEdit
          Min = 1
          Max = 15
          ParentShowHint = False
          Position = 1
          ShowHint = True
          TabOrder = 7
          OnChangingEx = OrnamentCopyToUpDownChangingEx
        end
      end
    end
    object OptTab: TTabSheet
      Caption = 'Options'
      ImageIndex = 21
      object VtmFeaturesGrp: TRadioGroup
        Left = 10
        Top = 16
        Width = 223
        Height = 105
        Caption = 'Features Level '
        ItemIndex = 1
        Items.Strings = (
          'Pro Tracker 3.5'
          'Vortex Tracker II (PT 3.6)'
          'Pro Tracker 3.7')
        TabOrder = 0
        OnClick = VtmFeaturesGrpClick
      end
      object SaveHead: TRadioGroup
        Left = 248
        Top = 16
        Width = 265
        Height = 105
        Caption = 'Save With Header '
        ItemIndex = 0
        Items.Strings = (
          '"Vortex Tracker II 2.0 module:" where possible'
          '"ProTracker 3.x compilation of" always')
        TabOrder = 1
        OnClick = SaveHeadClick
      end
      object GroupBox5: TGroupBox
        Left = 10
        Top = 136
        Width = 223
        Height = 97
        Caption = 'Frequency Table for Current Track '
        TabOrder = 2
        object Label8: TLabel
          Left = 10
          Top = 35
          Width = 82
          Height = 13
          Caption = 'Number of Table:'
        end
        object TableName: TLabel
          Left = 10
          Top = 63
          Width = 114
          Height = 13
          Caption = 'ASM or PSC (1.75 MHz)'
        end
        object Edit7: TEdit
          Left = 97
          Top = 32
          Width = 36
          Height = 21
          Hint = 'Note table'
          AutoSize = False
          TabOrder = 0
          Text = '2'
          OnChange = Edit7Change
          OnExit = Edit7Exit
        end
        object UpDown4: TUpDown
          Left = 133
          Top = 32
          Width = 16
          Height = 21
          Hint = 'Note table'
          Associate = Edit7
          Max = 4
          Position = 2
          TabOrder = 1
          OnChangingEx = UpDown4ChangingEx
        end
      end
    end
  end
  object SaveTextDlg: TSaveDialog
    DefaultExt = 'TXT'
    Filter = 'Sample files|*.vts|Text files|*.txt|All files|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofShareAware, ofEnableSizing]
    Left = 92
    Top = 505
  end
  object LoadTextDlg: TOpenDialog
    DefaultExt = 'TXT'
    Filter = 'Text files|*.txt|All files|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 60
    Top = 505
  end
  object ShowHintTimer: TTimer
    Enabled = False
    OnTimer = ShowHintTimerTimer
    Left = 160
    Top = 504
  end
  object HideHintTimer: TTimer
    Enabled = False
    OnTimer = HideHintTimerTimer
    Left = 200
    Top = 504
  end
  object ChangeBackupVersion: TTimer
    Interval = 1500000
    OnTimer = ChangeBackupVersionTimer
    Left = 240
    Top = 504
  end
  object ExportWavDialog: TSaveDialog
    DefaultExt = 'wav'
    Filter = 'WAV|*.wav'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofShareAware, ofEnableSizing]
    Left = 124
    Top = 505
  end
end
