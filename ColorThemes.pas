{
This is part of Vortex Tracker II project

Version 2.0 and later
(c)2017-2018 Ivan Pirog, ivan.pirog@gmail.com
}

unit ColorThemes;

interface

uses Classes, SysUtils, inifiles, Dialogs, Controls, Graphics, HotKeys, Windows;

type
  PRGBColor = ^TRGBColor;
  TRGBColor = string[7];

  TColorTheme = record
    Name: String;
    Background: TRGBColor;          // Main background color
    SelLineBackground: TRGBColor;   // Selected line bg color
    HighlBackground: TRGBColor;     // Highlighted lines bg color
    OutBackground: TRGBColor;       // Prev/Next pattern bg color
    OutHlBackground: TRGBColor;     // Prev/Next pattern highlighted lines bg color
    Text: TRGBColor;                // Text color (dots)
    SelLineText: TRGBColor;         // Selected line text color
    HighlText: TRGBColor;           // Highlighted text color (dots)
    OutText: TRGBColor;             // Prev/Next pattern text color
    LineNum: TRGBColor;             // Line numbers color
    SelLineNum: TRGBColor;          // Highlighted line numbers
    Envelope: TRGBColor;            // Envelope value color
    SelEnvelope: TRGBColor;         // Highlighted envelope
    Noise: TRGBColor;               // Noise value color
    SelNoise: TRGBColor;            // Highlighted noise
    Note: TRGBColor;                // Note color
    SelNote: TRGBColor;             // Highlighted notes
    NoteParams: TRGBColor;          // Note parameters color (Sample, Ornament, Volume)
    SelNoteParams: TRGBColor;       // Highlighted note params
    NoteCommands: TRGBColor;        // Note special commands color
    SelNoteCommands: TRGBColor;     // Highlighted note commands
    Separators: TRGBColor;          // Vertical separators color
    OutSeparators: TRGBColor;       // Prev/Next pattern separators color
    SamOrnBackground: TRGBColor;    // Sample/Ornament editor background
    SamOrnSelBackground:TRGBColor;  // Sample/Ornament selected line background
    SamOrnText: TRGBColor;          // Sample/Ornament text color
    SamOrnSelText: TRGBColor;       // Sample/Ornament selected line text color
    SamOrnLineNum: TRGBColor;       // Sample/Ornament line number
    SamOrnSelLineNum:TRGBColor;     // Sample/Ornament selected line number
    SamNoise: TRGBColor;            // Sample noise color
    SamSelNoise: TRGBColor;         // Sample selected line noise color
    SamOrnSeparators: TRGBColor;    // Sample/Ornament separators
    SamTone: TRGBColor;             // Sample tone shift color
    SamSelTone: TRGBColor;          // Sample selected line tone shift color
    FullScreenBackground:TRGBColor; // Fullscreen background color
  end;

  TThemesArray = array of string;

const

  BrightBG  = $333333;
  BrightHL  = $171717;
  BrightTXT = $525252;

  ThemeINIKey = 'Vortex Tracker 2.0 Theme';

  DefaultColorThemes : array[0..20] of TColorTheme = (
    (
      Name: 'Default';
      Background: TRGBColor('FDFCEF');
      SelLineBackground: TRGBColor('393E8C');
      HighlBackground: TRGBColor('FAE7D4');
      OutBackground: TRGBColor('FBFBE8');
      OutHlBackground: TRGBColor('EFEFCC');
      Text: TRGBColor('AC8576');
      SelLineText: TRGBColor('B7B85C');
      HighlText: TRGBColor('8F8B5B');
      OutText: TRGBColor('C18573');
      LineNum: TRGBColor('915D57');
      SelLineNum: TRGBColor('ECEDD1');
      Envelope: TRGBColor('5E4C45');
      SelEnvelope: TRGBColor('E1E4C8');
      Noise: TRGBColor('A4744E');
      SelNoise: TRGBColor('E1E4C8');
      Note: TRGBColor('00219D');
      SelNote: TRGBColor('E1E4C8');
      NoteParams: TRGBColor('7F665D');
      SelNoteParams: TRGBColor('E1E4C8');
      NoteCommands: TRGBColor('765F57');
      SelNoteCommands: TRGBColor('E1E4C8');
      Separators: TRGBColor('D9998D');
      OutSeparators: TRGBColor('B3B3A8');
      SamOrnBackground: TRGBColor('FCFCEC');
      SamOrnSelBackground: TRGBColor('424584');
      SamOrnText: TRGBColor('926858');
      SamOrnSelText: TRGBColor('714F44');
      SamOrnLineNum: TRGBColor('915D57');
      SamOrnSelLineNum: TRGBColor('F6F5D3');
      SamNoise: TRGBColor('915F5C');
      SamSelNoise: TRGBColor('63326A');
      SamOrnSeparators: TRGBColor('D9998D');
      SamTone: TRGBColor('604D47');
      SamSelTone: TRGBColor('24546E');
      FullScreenBackground: TRGBColor('221C1C');
    ),
    (
      Name: 'MmcM';
      Background: TRGBColor('000000');
      SelLineBackground: TRGBColor('0009A5');
      HighlBackground: TRGBColor('1D1D1D');
      OutBackground: TRGBColor('000000');
      OutHlBackground: TRGBColor('181818');
      Text: TRGBColor('808080');
      SelLineText: TRGBColor('CCCCCC');
      HighlText: TRGBColor('808080');
      OutText: TRGBColor('636363');
      LineNum: TRGBColor('7F7F7F');
      SelLineNum: TRGBColor('CCCCCC');
      Envelope: TRGBColor('06A6E1');
      SelEnvelope: TRGBColor('CCCCCC');
      Noise: TRGBColor('D47908');
      SelNoise: TRGBColor('CCCCCC');
      Note: TRGBColor('CCCCCC');
      SelNote: TRGBColor('CCCCCC');
      NoteParams: TRGBColor('00A30C');
      SelNoteParams: TRGBColor('CCCCCC');
      NoteCommands: TRGBColor('C9C804');
      SelNoteCommands: TRGBColor('CCCCCC');
      Separators: TRGBColor('717171');
      OutSeparators: TRGBColor('3C3C3C');
      SamOrnBackground: TRGBColor('000000');
      SamOrnSelBackground: TRGBColor('0009A5');
      SamOrnText: TRGBColor('CCCCCC');
      SamOrnSelText: TRGBColor('C6C6C6');
      SamOrnLineNum: TRGBColor('7B7B7B');
      SamOrnSelLineNum: TRGBColor('D4D4D4');
      SamNoise: TRGBColor('EE8809');
      SamSelNoise: TRGBColor('EE8809');
      SamOrnSeparators: TRGBColor('717171');
      SamTone: TRGBColor('59C40C');
      SamSelTone: TRGBColor('59C40C');
      FullScreenBackground: TRGBColor('001020');
    ),
    (
      Name: 'EA''s Theme';
      Background: TRGBColor('000000');
      SelLineBackground: TRGBColor('0000FF');
      HighlBackground: TRGBColor('000040');
      OutBackground: TRGBColor('000000');
      OutHlBackground: TRGBColor('000040');
      Text: TRGBColor('404040');
      SelLineText: TRGBColor('FFFFFF');
      HighlText: TRGBColor('404040');
      OutText: TRGBColor('404040');
      LineNum: TRGBColor('008000');
      SelLineNum: TRGBColor('FFFFFF');
      Envelope: TRGBColor('00FFFF');
      SelEnvelope: TRGBColor('FFFFFF');
      Noise: TRGBColor('FF8000');
      SelNoise: TRGBColor('FFFFFF');
      Note: TRGBColor('FFFFFF');
      SelNote: TRGBColor('FFFFFF');
      NoteParams: TRGBColor('00FF00');
      SelNoteParams: TRGBColor('FFFFFF');
      NoteCommands: TRGBColor('FFFF00');
      SelNoteCommands: TRGBColor('FFFFFF');
      Separators: TRGBColor('C0C0C0');
      OutSeparators: TRGBColor('404040');
      SamOrnBackground: TRGBColor('000000');
      SamOrnSelBackground: TRGBColor('FF0000');
      SamOrnText: TRGBColor('808080');
      SamOrnSelText: TRGBColor('FFFFFF');
      SamOrnLineNum: TRGBColor('008000');
      SamOrnSelLineNum: TRGBColor('EEF41F');
      SamNoise: TRGBColor('C08000');
      SamSelNoise: TRGBColor('FF8000');
      SamOrnSeparators: TRGBColor('C0C0C0');
      SamTone: TRGBColor('008000');
      SamSelTone: TRGBColor('00FF00');
      FullScreenBackground: TRGBColor('000000');
    ),
    (
      Name: TRGBColor('FruityLoops');
      Background: TRGBColor('34444E');
      SelLineBackground: TRGBColor('29363D');
      HighlBackground: TRGBColor('3D4E59');
      OutBackground: TRGBColor('3A4853');
      OutHlBackground: TRGBColor('3E4F5D');
      Text: TRGBColor('4D565B');
      SelLineText: TRGBColor('CFDEEA');
      HighlText: TRGBColor('515152');
      OutText: TRGBColor('626D73');
      LineNum: TRGBColor('66909C');
      SelLineNum: TRGBColor('A6C3C3');
      Envelope: TRGBColor('576693');
      SelEnvelope: TRGBColor('778BB1');
      Noise: TRGBColor('576693');
      SelNoise: TRGBColor('778BB1');
      Note: TRGBColor('A3A8AB');
      SelNote: TRGBColor('D8E8F1');
      NoteParams: TRGBColor('626A72');
      SelNoteParams: TRGBColor('B997AC');
      NoteCommands: TRGBColor('5C6E70');
      SelNoteCommands: TRGBColor('A1CBB2');
      Separators: TRGBColor('5F686D');
      OutSeparators: TRGBColor('5F686D');
      SamOrnBackground: TRGBColor('29363D');
      SamOrnSelBackground: TRGBColor('34444E');
      SamOrnText: TRGBColor('66909C');
      SamOrnSelText: TRGBColor('CFDEEA');
      SamOrnLineNum: TRGBColor('669C8B');
      SamOrnSelLineNum: TRGBColor('B9D0D0');
      SamNoise: TRGBColor('576693');
      SamSelNoise: TRGBColor('778BB1');
      SamOrnSeparators: TRGBColor('5F686D');
      SamTone: TRGBColor('626A72');
      SamSelTone: TRGBColor('B997AC');
      FullScreenBackground: TRGBColor('34444E');
    ),
    (
      Name: 'Aqua';
      Background: TRGBColor('0C4A5D');
      SelLineBackground: TRGBColor('059BD7');
      HighlBackground: TRGBColor('1A6279');
      OutBackground: TRGBColor('0A3D4D');
      OutHlBackground: TRGBColor('0E4F64');
      Text: TRGBColor('0FDADE');
      SelLineText: TRGBColor('FFFFFF');
      HighlText: TRGBColor('0FDADE');
      OutText: TRGBColor('25919F');
      LineNum: TRGBColor('6FC6E4');
      SelLineNum: TRGBColor('FFFFFF');
      Envelope: TRGBColor('00F7D9');
      SelEnvelope: TRGBColor('FFFFFF');
      Noise: TRGBColor('F9B93F');
      SelNoise: TRGBColor('FFFFFF');
      Note: TRGBColor('FFFFFF');
      SelNote: TRGBColor('FFFFFF');
      NoteParams: TRGBColor('6AF573');
      SelNoteParams: TRGBColor('FFFFFF');
      NoteCommands: TRGBColor('E0DE04');
      SelNoteCommands: TRGBColor('FFFFFF');
      Separators: TRGBColor('DFDFDF');
      OutSeparators: TRGBColor('DFDFDF');
      SamOrnBackground: TRGBColor('09455E');
      SamOrnSelBackground: TRGBColor('0F617A');
      SamOrnText: TRGBColor('0FDADE');
      SamOrnSelText: TRGBColor('FFFFFF');
      SamOrnLineNum: TRGBColor('9CF8BB');
      SamOrnSelLineNum: TRGBColor('FFFFFF');
      SamNoise: TRGBColor('6AF573');
      SamSelNoise: TRGBColor('BDF56D');
      SamOrnSeparators: TRGBColor('DFDFDF');
      SamTone: TRGBColor('E0DE04');
      SamSelTone: TRGBColor('FBFA2F');
      FullScreenBackground: TRGBColor('072C38');
    ),
    (
      Name: 'Flexx';
      Background: TRGBColor('131313');
      SelLineBackground: TRGBColor('42387A');
      HighlBackground: TRGBColor('1D1C18');
      OutBackground: TRGBColor('161616');
      OutHlBackground: TRGBColor('1E1D19');
      Text: TRGBColor('00570C');
      SelLineText: TRGBColor('B3B3B3');
      HighlText: TRGBColor('515152');
      OutText: TRGBColor('3C3926');
      LineNum: TRGBColor('3F6B93');
      SelLineNum: TRGBColor('FFFFFF');
      Envelope: TRGBColor('FA7351');
      SelEnvelope: TRGBColor('DAD9E4');
      Noise: TRGBColor('D3CCB2');
      SelNoise: TRGBColor('D8D2BB');
      Note: TRGBColor('9391F4');
      SelNote: TRGBColor('EEEEC8');
      NoteParams: TRGBColor('BF6240');
      SelNoteParams: TRGBColor('E8AB95');
      NoteCommands: TRGBColor('35A36D');
      SelNoteCommands: TRGBColor('A1CBB2');
      Separators: TRGBColor('64675B');
      OutSeparators: TRGBColor('393B34');
      SamOrnBackground: TRGBColor('131313');
      SamOrnSelBackground: TRGBColor('3E306D');
      SamOrnText: TRGBColor('B4B591');
      SamOrnSelText: TRGBColor('BFB398');
      SamOrnLineNum: TRGBColor('3F675D');
      SamOrnSelLineNum: TRGBColor('CFDA9C');
      SamNoise: TRGBColor('8584A8');
      SamSelNoise: TRGBColor('9CABCA');
      SamOrnSeparators: TRGBColor('64675B');
      SamTone: TRGBColor('C77759');
      SamSelTone: TRGBColor('D8C075');
      FullScreenBackground: TRGBColor('0C0C0C');
    ),
    (
      Name: 'Quiet';
      Background: TRGBColor('181B18');
      SelLineBackground: TRGBColor('17BCD9');
      HighlBackground: TRGBColor('282828');
      OutBackground: TRGBColor('191919');
      OutHlBackground: TRGBColor('262626');
      Text: TRGBColor('10991C');
      SelLineText: TRGBColor('FFFFFF');
      HighlText: TRGBColor('515152');
      OutText: TRGBColor('717171');
      LineNum: TRGBColor('448265');
      SelLineNum: TRGBColor('FFFFFF');
      Envelope: TRGBColor('16DA28');
      SelEnvelope: TRGBColor('FFFFFF');
      Noise: TRGBColor('EFF518');
      SelNoise: TRGBColor('FFFFFF');
      Note: TRGBColor('16DA28');
      SelNote: TRGBColor('FFFFFF');
      NoteParams: TRGBColor('76D2E2');
      SelNoteParams: TRGBColor('FFFFFF');
      NoteCommands: TRGBColor('B65FFC');
      SelNoteCommands: TRGBColor('FFFFFF');
      Separators: TRGBColor('31BDC1');
      OutSeparators: TRGBColor('434343');
      SamOrnBackground: TRGBColor('181B18');
      SamOrnSelBackground: TRGBColor('2C2C2C');
      SamOrnText: TRGBColor('63A569');
      SamOrnSelText: TRGBColor('85A889');
      SamOrnLineNum: TRGBColor('3F675D');
      SamOrnSelLineNum: TRGBColor('7BC6B8');
      SamNoise: TRGBColor('A9AC3A');
      SamSelNoise: TRGBColor('D3D74D');
      SamOrnSeparators: TRGBColor('256462');
      SamTone: TRGBColor('9270AE');
      SamSelTone: TRGBColor('AF88CF');
      FullScreenBackground: TRGBColor('141614');
    ),
    (
      Name: 'OpenMPT';
      Background: TRGBColor('FAFAFA');
      SelLineBackground: TRGBColor('2B2B2B');
      HighlBackground: TRGBColor('EEEEEE');
      OutBackground: TRGBColor('E3E3DC');
      OutHlBackground: TRGBColor('D4D4CC');
      Text: TRGBColor('8A8A8A');
      SelLineText: TRGBColor('FFFFFF');
      HighlText: TRGBColor('757575');
      OutText: TRGBColor('ABABAB');
      LineNum: TRGBColor('374D5B');
      SelLineNum: TRGBColor('FFFFFF');
      Envelope: TRGBColor('714D47');
      SelEnvelope: TRGBColor('FFFFFF');
      Noise: TRGBColor('617B4C');
      SelNoise: TRGBColor('FFFFFF');
      Note: TRGBColor('38386E');
      SelNote: TRGBColor('FFFFFF');
      NoteParams: TRGBColor('458C3B');
      SelNoteParams: TRGBColor('FFFFFF');
      NoteCommands: TRGBColor('86880A');
      SelNoteCommands: TRGBColor('FFFFFF');
      Separators: TRGBColor('CBCBCB');
      OutSeparators: TRGBColor('C7C7BF');
      SamOrnBackground: TRGBColor('FAFAFA');
      SamOrnSelBackground: TRGBColor('3C3C3C');
      SamOrnText: TRGBColor('8A8A8A');
      SamOrnSelText: TRGBColor('747474');
      SamOrnLineNum: TRGBColor('40596A');
      SamOrnSelLineNum: TRGBColor('BFD8D1');
      SamNoise: TRGBColor('458C3B');
      SamSelNoise: TRGBColor('7B750F');
      SamOrnSeparators: TRGBColor('A9A99D');
      SamTone: TRGBColor('565686');
      SamSelTone: TRGBColor('57577E');
      FullScreenBackground: TRGBColor('607179');
    ),
    (
      Name: 'Bfox';
      Background: TRGBColor('000000');
      SelLineBackground: TRGBColor('0000E6');
      HighlBackground: TRGBColor('101010');
      OutBackground: TRGBColor('000000');
      OutHlBackground: TRGBColor('0F0F0F');
      Text: TRGBColor('005000');
      SelLineText: TRGBColor('FEFEFE');
      HighlText: TRGBColor('005000');
      OutText: TRGBColor('303030');
      LineNum: TRGBColor('00CC00');
      SelLineNum: TRGBColor('FEFEFE');
      Envelope: TRGBColor('009D00');
      SelEnvelope: TRGBColor('FEFEFE');
      Noise: TRGBColor('009D00');
      SelNoise: TRGBColor('FEFEFE');
      Note: TRGBColor('00CC00');
      SelNote: TRGBColor('FEFEFE');
      NoteParams: TRGBColor('009D00');
      SelNoteParams: TRGBColor('FEFEFE');
      NoteCommands: TRGBColor('009D00');
      SelNoteCommands: TRGBColor('FEFEFE');
      Separators: TRGBColor('9D9D9D');
      OutSeparators: TRGBColor('202020');
      SamOrnBackground: TRGBColor('000000');
      SamOrnSelBackground: TRGBColor('7840CC');
      SamOrnText: TRGBColor('CCCCCC');
      SamOrnSelText: TRGBColor('E9E9E9');
      SamOrnLineNum: TRGBColor('00A8A8');
      SamOrnSelLineNum: TRGBColor('CCCCCC');
      SamNoise: TRGBColor('CCCC00');
      SamSelNoise: TRGBColor('E9E900');
      SamOrnSeparators: TRGBColor('9D9D9D');
      SamTone: TRGBColor('CCCCCC');
      SamSelTone: TRGBColor('FEFEFE');
      FullScreenBackground: TRGBColor('030403');
    ),
    (
      Name: 'PT2 Scheme';
      Background: TRGBColor('000000');
      SelLineBackground: TRGBColor('000000');
      HighlBackground: TRGBColor('181818');
      OutBackground: TRGBColor('000000');
      OutHlBackground: TRGBColor('0F0F0F');
      Text: TRGBColor('007800');
      SelLineText: TRGBColor('CCCCCC');
      HighlText: TRGBColor('007800');
      OutText: TRGBColor('303030');
      LineNum: TRGBColor('00CC00');
      SelLineNum: TRGBColor('FEFEFE');
      Envelope: TRGBColor('00CCCC');
      SelEnvelope: TRGBColor('FEFEFE');
      Noise: TRGBColor('CCCC00');
      SelNoise: TRGBColor('FEFEFE');
      Note: TRGBColor('00CC00');
      SelNote: TRGBColor('FEFEFE');
      NoteParams: TRGBColor('00CC00');
      SelNoteParams: TRGBColor('FEFEFE');
      NoteCommands: TRGBColor('00CC00');
      SelNoteCommands: TRGBColor('FEFEFE');
      Separators: TRGBColor('CCCCCC');
      OutSeparators: TRGBColor('3A3A3A');
      SamOrnBackground: TRGBColor('000000');
      SamOrnSelBackground: TRGBColor('CCCCCC');
      SamOrnText: TRGBColor('CCCCCC');
      SamOrnSelText: TRGBColor('E9E9E9');
      SamOrnLineNum: TRGBColor('00CC00');
      SamOrnSelLineNum: TRGBColor('000000');
      SamNoise: TRGBColor('CCCC00');
      SamSelNoise: TRGBColor('E9E900');
      SamOrnSeparators: TRGBColor('787878');
      SamTone: TRGBColor('00CCCC');
      SamSelTone: TRGBColor('00E9E9');
      FullScreenBackground: TRGBColor('090909');
    ),
    (
      Name: 'PT3 Green';
      Background: TRGBColor('000000');
      SelLineBackground: TRGBColor('0000FE');
      HighlBackground: TRGBColor('141414');
      OutBackground: TRGBColor('000000');
      OutHlBackground: TRGBColor('0F0F0F');
      Text: TRGBColor('009D00');
      SelLineText: TRGBColor('FEFEFE');
      HighlText: TRGBColor('009D00');
      OutText: TRGBColor('383838');
      LineNum: TRGBColor('00FE00');
      SelLineNum: TRGBColor('FEFEFE');
      Envelope: TRGBColor('00CC00');
      SelEnvelope: TRGBColor('FEFEFE');
      Noise: TRGBColor('00CC00');
      SelNoise: TRGBColor('FEFEFE');
      Note: TRGBColor('00CC00');
      SelNote: TRGBColor('FEFEFE');
      NoteParams: TRGBColor('00CC00');
      SelNoteParams: TRGBColor('FEFEFE');
      NoteCommands: TRGBColor('00CC00');
      SelNoteCommands: TRGBColor('FEFEFE');
      Separators: TRGBColor('CCCCCC');
      OutSeparators: TRGBColor('3A3A3A');
      SamOrnBackground: TRGBColor('000000');
      SamOrnSelBackground: TRGBColor('CC00CC');
      SamOrnText: TRGBColor('CCCCCC');
      SamOrnSelText: TRGBColor('CCCCCC');
      SamOrnLineNum: TRGBColor('00CCCC');
      SamOrnSelLineNum: TRGBColor('CCCCCC');
      SamNoise: TRGBColor('CCCCCC');
      SamSelNoise: TRGBColor('CCCCCC');
      SamOrnSeparators: TRGBColor('CCCCCC');
      SamTone: TRGBColor('CCCCCC');
      SamSelTone: TRGBColor('CCCCCC');
      FullScreenBackground: TRGBColor('090909');
    ),
    (
      Name: 'Iodine';
      Background: TRGBColor('F5F5E0');
      SelLineBackground: TRGBColor('7A4421');
      HighlBackground: TRGBColor('EBE8C5');
      OutBackground: TRGBColor('F3F3E1');
      OutHlBackground: TRGBColor('EEECCE');
      Text: TRGBColor('AEA265');
      SelLineText: TRGBColor('AFA467');
      HighlText: TRGBColor('9C9B54');
      OutText: TRGBColor('C4AB72');
      LineNum: TRGBColor('50965A');
      SelLineNum: TRGBColor('FFFAFA');
      Envelope: TRGBColor('A09D64');
      SelEnvelope: TRGBColor('F3E2AF');
      Noise: TRGBColor('839D5B');
      SelNoise: TRGBColor('DCD7C7');
      Note: TRGBColor('774913');
      SelNote: TRGBColor('F3C7B4');
      NoteParams: TRGBColor('B05784');
      SelNoteParams: TRGBColor('D2CBB1');
      NoteCommands: TRGBColor('A86E79');
      SelNoteCommands: TRGBColor('B7CEE6');
      Separators: TRGBColor('F6A07D');
      OutSeparators: TRGBColor('F9BEA6');
      SamOrnBackground: TRGBColor('F5F5E0');
      SamOrnSelBackground: TRGBColor('A46520');
      SamOrnText: TRGBColor('B09172');
      SamOrnSelText: TRGBColor('917F42');
      SamOrnLineNum: TRGBColor('50965A');
      SamOrnSelLineNum: TRGBColor('FFFFFF');
      SamNoise: TRGBColor('3C8F70');
      SamSelNoise: TRGBColor('437F68');
      SamOrnSeparators: TRGBColor('C29E5E');
      SamTone: TRGBColor('B96733');
      SamSelTone: TRGBColor('A95119');
      FullScreenBackground: TRGBColor('181412');
    ),
    (
      Name: 'Grayscaled Light';
      Background: TRGBColor('FFFFFF');
      SelLineBackground: TRGBColor('666666');
      HighlBackground: TRGBColor('EFEFEF');
      OutBackground: TRGBColor('E5E5E5');
      OutHlBackground: TRGBColor('D4D4D4');
      Text: TRGBColor('929292');
      SelLineText: TRGBColor('CCCCCC');
      HighlText: TRGBColor('4F595F');
      OutText: TRGBColor('ACACAC');
      LineNum: TRGBColor('6C6C6C');
      SelLineNum: TRGBColor('DFDFDF');
      Envelope: TRGBColor('606060');
      SelEnvelope: TRGBColor('E0E0E0');
      Noise: TRGBColor('585858');
      SelNoise: TRGBColor('F7F7F7');
      Note: TRGBColor('383838');
      SelNote: TRGBColor('FFFFFF');
      NoteParams: TRGBColor('696969');
      SelNoteParams: TRGBColor('EAEAEA');
      NoteCommands: TRGBColor('7C7C7C');
      SelNoteCommands: TRGBColor('D2D2D2');
      Separators: TRGBColor('909090');
      OutSeparators: TRGBColor('ACACAC');
      SamOrnBackground: TRGBColor('FFFFFF');
      SamOrnSelBackground: TRGBColor('787878');
      SamOrnText: TRGBColor('6F6F6F');
      SamOrnSelText: TRGBColor('636363');
      SamOrnLineNum: TRGBColor('7E7E7E');
      SamOrnSelLineNum: TRGBColor('ECECEC');
      SamNoise: TRGBColor('484848');
      SamSelNoise: TRGBColor('3C3C3C');
      SamOrnSeparators: TRGBColor('909090');
      SamTone: TRGBColor('5C5C5C');
      SamSelTone: TRGBColor('474747');
      FullScreenBackground: TRGBColor('737373');
    ),
    (
      Name: 'Grayscaled Dark';
      Background: TRGBColor('131313');
      SelLineBackground: TRGBColor('3B3B3B');
      HighlBackground: TRGBColor('191919');
      OutBackground: TRGBColor('0B0B0B');
      OutHlBackground: TRGBColor('1A1A1A');
      Text: TRGBColor('323232');
      SelLineText: TRGBColor('CCCCCC');
      HighlText: TRGBColor('424B50');
      OutText: TRGBColor('323232');
      LineNum: TRGBColor('515151');
      SelLineNum: TRGBColor('DFDFDF');
      Envelope: TRGBColor('606060');
      SelEnvelope: TRGBColor('B4B4B4');
      Noise: TRGBColor('585858');
      SelNoise: TRGBColor('B6B6B6');
      Note: TRGBColor('C5C5C5');
      SelNote: TRGBColor('FFFFFF');
      NoteParams: TRGBColor('696969');
      SelNoteParams: TRGBColor('CACACA');
      NoteCommands: TRGBColor('565656');
      SelNoteCommands: TRGBColor('CACACA');
      Separators: TRGBColor('3A3A3A');
      OutSeparators: TRGBColor('353535');
      SamOrnBackground: TRGBColor('131313');
      SamOrnSelBackground: TRGBColor('393939');
      SamOrnText: TRGBColor('A1A1A1');
      SamOrnSelText: TRGBColor('8B8B8B');
      SamOrnLineNum: TRGBColor('BABABA');
      SamOrnSelLineNum: TRGBColor('909090');
      SamNoise: TRGBColor('B1B1B1');
      SamSelNoise: TRGBColor('BFBFBF');
      SamOrnSeparators: TRGBColor('4E4E4E');
      SamTone: TRGBColor('9E9E9E');
      SamSelTone: TRGBColor('BABABA');
      FullScreenBackground: TRGBColor('101010');
    ),
    (
      Name: 'Dark Blue';
      Background: TRGBColor('101021');
      SelLineBackground: TRGBColor('242478');
      HighlBackground: TRGBColor('141428');
      OutBackground: TRGBColor('0D0D1B');
      OutHlBackground: TRGBColor('0B0B16');
      Text: TRGBColor('2C2739');
      SelLineText: TRGBColor('918581');
      HighlText: TRGBColor('424B50');
      OutText: TRGBColor('2D3748');
      LineNum: TRGBColor('63494D');
      SelLineNum: TRGBColor('DFDFDF');
      Envelope: TRGBColor('466378');
      SelEnvelope: TRGBColor('B4B4B4');
      Noise: TRGBColor('426A5A');
      SelNoise: TRGBColor('B6B6B6');
      Note: TRGBColor('CCCCD3');
      SelNote: TRGBColor('FFFFFF');
      NoteParams: TRGBColor('604035');
      SelNoteParams: TRGBColor('CACACA');
      NoteCommands: TRGBColor('734D69');
      SelNoteCommands: TRGBColor('CACACA');
      Separators: TRGBColor('242139');
      OutSeparators: TRGBColor('1E1B2F');
      SamOrnBackground: TRGBColor('101021');
      SamOrnSelBackground: TRGBColor('1C1C36');
      SamOrnText: TRGBColor('837F90');
      SamOrnSelText: TRGBColor('A0A2B1');
      SamOrnLineNum: TRGBColor('63494D');
      SamOrnSelLineNum: TRGBColor('8E696E');
      SamNoise: TRGBColor('4F7448');
      SamSelNoise: TRGBColor('6B9962');
      SamOrnSeparators: TRGBColor('3F3B58');
      SamTone: TRGBColor('764F41');
      SamSelTone: TRGBColor('A36E5A');
      FullScreenBackground: TRGBColor('0B0B1A');
    ),
    (
      Name: 'Burgundi';
      Background: TRGBColor('211010');
      SelLineBackground: TRGBColor('772222');
      HighlBackground: TRGBColor('281414');
      OutBackground: TRGBColor('180B0B');
      OutHlBackground: TRGBColor('1D1010');
      Text: TRGBColor('392927');
      SelLineText: TRGBColor('8D5E4D');
      HighlText: TRGBColor('433737');
      OutText: TRGBColor('3F251E');
      LineNum: TRGBColor('914237');
      SelLineNum: TRGBColor('AED697');
      Envelope: TRGBColor('81814B');
      SelEnvelope: TRGBColor('B4B4B4');
      Noise: TRGBColor('756537');
      SelNoise: TRGBColor('B89878');
      Note: TRGBColor('E8C64D');
      SelNote: TRGBColor('E9C54C');
      NoteParams: TRGBColor('AA654B');
      SelNoteParams: TRGBColor('CB8F79');
      NoteCommands: TRGBColor('4F6C3D');
      SelNoteCommands: TRGBColor('83B761');
      Separators: TRGBColor('402525');
      OutSeparators: TRGBColor('361E1E');
      SamOrnBackground: TRGBColor('211010');
      SamOrnSelBackground: TRGBColor('611919');
      SamOrnText: TRGBColor('B68774');
      SamOrnSelText: TRGBColor('C5A094');
      SamOrnLineNum: TRGBColor('773C34');
      SamOrnSelLineNum: TRGBColor('C1A173');
      SamNoise: TRGBColor('AA654B');
      SamSelNoise: TRGBColor('BE6C4E');
      SamOrnSeparators: TRGBColor('3D2323');
      SamTone: TRGBColor('AE7F43');
      SamSelTone: TRGBColor('C38940');
      FullScreenBackground: TRGBColor('120D0D');
    ),
    (
      Name: 'Night Mode';
      Background: TRGBColor('060906');
      SelLineBackground: TRGBColor('0F202D');
      HighlBackground: TRGBColor('0B1315');
      OutBackground: TRGBColor('060906');
      OutHlBackground: TRGBColor('080E10');
      Text: TRGBColor('172825');
      SelLineText: TRGBColor('8D5E4D');
      HighlText: TRGBColor('283431');
      OutText: TRGBColor('192329');
      LineNum: TRGBColor('1F4240');
      SelLineNum: TRGBColor('2B706F');
      Envelope: TRGBColor('896241');
      SelEnvelope: TRGBColor('B8875D');
      Noise: TRGBColor('756537');
      SelNoise: TRGBColor('B89878');
      Note: TRGBColor('E7C731');
      SelNote: TRGBColor('E9C54C');
      NoteParams: TRGBColor('476B32');
      SelNoteParams: TRGBColor('5B8A40');
      NoteCommands: TRGBColor('543B31');
      SelNoteCommands: TRGBColor('8D6453');
      Separators: TRGBColor('1A2B2D');
      OutSeparators: TRGBColor('152324');
      SamOrnBackground: TRGBColor('060906');
      SamOrnSelBackground: TRGBColor('432718');
      SamOrnText: TRGBColor('62AD6B');
      SamOrnSelText: TRGBColor('71C77F');
      SamOrnLineNum: TRGBColor('4B4F25');
      SamOrnSelLineNum: TRGBColor('AB8E4D');
      SamNoise: TRGBColor('AA654B');
      SamSelNoise: TRGBColor('BE6C4E');
      SamOrnSeparators: TRGBColor('1A2B2D');
      SamTone: TRGBColor('517A39');
      SamSelTone: TRGBColor('6CA04A');
      FullScreenBackground: TRGBColor('060906');
    ),
    (
      Name: 'Night Mode 2';
      Background: TRGBColor('0B0C1B');
      SelLineBackground: TRGBColor('521C1C');
      HighlBackground: TRGBColor('161627');
      OutBackground: TRGBColor('080911');
      OutHlBackground: TRGBColor('12121B');
      Text: TRGBColor('172825');
      SelLineText: TRGBColor('8D5E4D');
      HighlText: TRGBColor('283431');
      OutText: TRGBColor('443727');
      LineNum: TRGBColor('8C7545');
      SelLineNum: TRGBColor('C7C870');
      Envelope: TRGBColor('7792A6');
      SelEnvelope: TRGBColor('91A8B9');
      Noise: TRGBColor('786838');
      SelNoise: TRGBColor('B89878');
      Note: TRGBColor('E0E542');
      SelNote: TRGBColor('EDCF69');
      NoteParams: TRGBColor('6F8B42');
      SelNoteParams: TRGBColor('947A46');
      NoteCommands: TRGBColor('A97049');
      SelNoteCommands: TRGBColor('A97049');
      Separators: TRGBColor('4D302A');
      OutSeparators: TRGBColor('2D2423');
      SamOrnBackground: TRGBColor('0B0C1B');
      SamOrnSelBackground: TRGBColor('521C1C');
      SamOrnText: TRGBColor('AC7D6C');
      SamOrnSelText: TRGBColor('CAC34D');
      SamOrnLineNum: TRGBColor('733838');
      SamOrnSelLineNum: TRGBColor('BFB756');
      SamNoise: TRGBColor('B5B92A');
      SamSelNoise: TRGBColor('DDE32D');
      SamOrnSeparators: TRGBColor('4D302A');
      SamTone: TRGBColor('B9662E');
      SamSelTone: TRGBColor('D48048');
      FullScreenBackground: TRGBColor('060906');
    ),
    (
      Name: TRGBColor('School Notebook');
      Background: TRGBColor('F2F1FE');
      SelLineBackground: TRGBColor('414273');
      HighlBackground: TRGBColor('E0DEFB');
      OutBackground: TRGBColor('F2F1FE');
      OutHlBackground: TRGBColor('E7E6FC');
      Text: TRGBColor('8E8F9F');
      SelLineText: TRGBColor('7175B1');
      HighlText: TRGBColor('767996');
      OutText: TRGBColor('A2A2BD');
      LineNum: TRGBColor('7A7995');
      SelLineNum: TRGBColor('E5E9F5');
      Envelope: TRGBColor('B26853');
      SelEnvelope: TRGBColor('F6DAC3');
      Noise: TRGBColor('756537');
      SelNoise: TRGBColor('B89878');
      Note: TRGBColor('524EC8');
      SelNote: TRGBColor('FCFCF2');
      NoteParams: TRGBColor('A5693D');
      SelNoteParams: TRGBColor('D8B296');
      NoteCommands: TRGBColor('A75B83');
      SelNoteCommands: TRGBColor('E8B7D1');
      Separators: TRGBColor('BAB7DB');
      OutSeparators: TRGBColor('C0BEDE');
      SamOrnBackground: TRGBColor('FFFFFF');
      SamOrnSelBackground: TRGBColor('FFFE8B');
      SamOrnText: TRGBColor('62A4C3');
      SamOrnSelText: TRGBColor('519ABC');
      SamOrnLineNum: TRGBColor('658DB6');
      SamOrnSelLineNum: TRGBColor('337FCE');
      SamNoise: TRGBColor('B26853');
      SamSelNoise: TRGBColor('CD421B');
      SamOrnSeparators: TRGBColor('BAB7DB');
      SamTone: TRGBColor('6866A8');
      SamSelTone: TRGBColor('615FAE');
      FullScreenBackground: TRGBColor('3D3D5D');
    ),
    (
      Name: TRGBColor('Red Wine');
      Background: TRGBColor('47162F');
      SelLineBackground: TRGBColor('AA1515');
      HighlBackground: TRGBColor('3B132B');
      OutBackground: TRGBColor('39112A');
      OutHlBackground: TRGBColor('331025');
      Text: TRGBColor('803030');
      SelLineText: TRGBColor('A9A62F');
      HighlText: TRGBColor('742C2C');
      OutText: TRGBColor('A25C33');
      LineNum: TRGBColor('CE6262');
      SelLineNum: TRGBColor('FCF9B2');
      Envelope: TRGBColor('BFDE40');
      SelEnvelope: TRGBColor('FCF9B2');
      Noise: TRGBColor('E6D689');
      SelNoise: TRGBColor('FCF9B2');
      Note: TRGBColor('FCFF0B');
      SelNote: TRGBColor('FFFD9C');
      NoteParams: TRGBColor('CD751C');
      SelNoteParams: TRGBColor('FFFD9C');
      NoteCommands: TRGBColor('BF841A');
      SelNoteCommands: TRGBColor('FFFD9C');
      Separators: TRGBColor('C85858');
      OutSeparators: TRGBColor('C65151');
      SamOrnBackground: TRGBColor('47162F');
      SamOrnSelBackground: TRGBColor('9A1825');
      SamOrnText: TRGBColor('E3B83C');
      SamOrnSelText: TRGBColor('F4FA1E');
      SamOrnLineNum: TRGBColor('A57211');
      SamOrnSelLineNum: TRGBColor('FDFCD2');
      SamNoise: TRGBColor('E69746');
      SamSelNoise: TRGBColor('F79837');
      SamOrnSeparators: TRGBColor('C85858');
      SamTone: TRGBColor('ADBDBE');
      SamSelTone: TRGBColor('D9F0EF');
      FullScreenBackground: TRGBColor('1D0B1E');
    ),
    (
      Name: 'Green State';
      Background: TRGBColor('16492E');
      SelLineBackground: TRGBColor('288B21');
      HighlBackground: TRGBColor('1D392A');
      OutBackground: TRGBColor('14432A');
      OutHlBackground: TRGBColor('123C26');
      Text: TRGBColor('627A5F');
      SelLineText: TRGBColor('7CD568');
      HighlText: TRGBColor('586D54');
      OutText: TRGBColor('73821E');
      LineNum: TRGBColor('AEAB6B');
      SelLineNum: TRGBColor('FEFEEC');
      Envelope: TRGBColor('7CDE40');
      SelEnvelope: TRGBColor('FEFEEC');
      Noise: TRGBColor('E6CB89');
      SelNoise: TRGBColor('FEFEEC');
      Note: TRGBColor('FFFEF4');
      SelNote: TRGBColor('FEFEEC');
      NoteParams: TRGBColor('F1C135');
      SelNoteParams: TRGBColor('FEFEEC');
      NoteCommands: TRGBColor('C2C123');
      SelNoteCommands: TRGBColor('FEFEEC');
      Separators: TRGBColor('37663A');
      OutSeparators: TRGBColor('37663A');
      SamOrnBackground: TRGBColor('16492E');
      SamOrnSelBackground: TRGBColor('348E24');
      SamOrnText: TRGBColor('F3F8E5');
      SamOrnSelText: TRGBColor('EEEBC0');
      SamOrnLineNum: TRGBColor('9CF8BB');
      SamOrnSelLineNum: TRGBColor('F9E5AA');
      SamNoise: TRGBColor('FCBE64');
      SamSelNoise: TRGBColor('FEE3BA');
      SamOrnSeparators: TRGBColor('38693B');
      SamTone: TRGBColor('FDFC73');
      SamSelTone: TRGBColor('FDFC97');
      FullScreenBackground: TRGBColor('072C38');
     )
  );


  WinColors: array[0..22] of Integer = (
    COLOR_SCROLLBAR,  COLOR_MENU, COLOR_MENUTEXT, COLOR_MENUHILIGHT,
    COLOR_MENUBAR, COLOR_WINDOW, COLOR_WINDOWFRAME, COLOR_WINDOWTEXT,
    COLOR_ACTIVEBORDER,COLOR_INACTIVEBORDER, COLOR_APPWORKSPACE,
    COLOR_HIGHLIGHT, COLOR_HIGHLIGHTTEXT, COLOR_BTNFACE, COLOR_BTNSHADOW,
    COLOR_BTNTEXT, COLOR_BTNHIGHLIGHT, COLOR_3DLIGHT,
    COLOR_GRAYTEXT, COLOR_3DDKSHADOW, COLOR_HOTLIGHT,
    COLOR_INFOTEXT, COLOR_INFOBK
  );

  WinColorThemes: array[0..6] of array[0..22] of TColor = (
    (
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ),
    (
      TColor($00C8D0D4),     // COLOR_SCROLLBAR
      TColor($00E6E3E1),     // COLOR_MENU
      TColor($002D2319),     // COLOR_MENUTEXT
      TColor($007D5F3C),     // COLOR_MENUHILIGHT
      TColor($00E6E3E1),     // COLOR_MENUBAR
      TColor($00F0F0F0),     // COLOR_WINDOW
      TColor($00000000),     // COLOR_WINDOWFRAME
      TColor($002D2319),     // COLOR_WINDOWTEXT
      TColor($00321900),     // COLOR_ACTIVEBORDER
      TColor($00321900),     // COLOR_INACTIVEBORDER
      TColor($00808080),     // COLOR_APPWORKSPACE
      TColor($00735532),     // COLOR_HIGHLIGHT
      TColor($00FFF5EB),     // COLOR_HIGHLIGHTTEXT
      TColor($00E6E3E1),     // COLOR_BTNFACE
      TColor($00D2CFCD),     // COLOR_BTNSHADOW
      TColor($00000000),     // COLOR_BTNTEXT
      TColor($00FAF7F5),     // COLOR_BTNHIGHLIGHT
      TColor($00F5EBE1),     // COLOR_3DLIGHT
      TColor($00AAA096),     // COLOR_GRAYTEXT
      TColor($00C8C5C3),     // COLOR_3DDKSHADOW
      TColor($007D5F3C),     // COLOR_HOTLIGHT
      TColor($00000000),     // COLOR_INFOTEXT
      TColor($00E1FFFF)      // COLOR_INFOBK
    ),
    (
      TColor($00C8D0D4),     // COLOR_SCROLLBAR
      TColor($00E5EDF0),     // COLOR_MENU
      TColor($00000000),     // COLOR_MENUTEXT
      TColor($00958370),     // COLOR_MENUHILIGHT
      TColor($00E5EDF0),     // COLOR_MENUBAR
      TColor($00FBFBFB),     // COLOR_WINDOW
      TColor($00000000),     // COLOR_WINDOWFRAME
      TColor($00000000),     // COLOR_WINDOWTEXT
      TColor($00C8D0D4),     // COLOR_ACTIVEBORDER
      TColor($00C8D0D4),     // COLOR_INACTIVEBORDER
      TColor($00808080),     // COLOR_APPWORKSPACE
      TColor($00958370),     // COLOR_HIGHLIGHT
      TColor($00F5F5F5),     // COLOR_HIGHLIGHTTEXT
      TColor($00E5EDF0),     // COLOR_BTNFACE
      TColor($00C2CDD4),     // COLOR_BTNSHADOW
      TColor($00000000),     // COLOR_BTNTEXT
      TColor($00FAFAFA),     // COLOR_BTNHIGHLIGHT
      TColor($00E9E9E9),     // COLOR_3DLIGHT
      TColor($00969696),     // COLOR_GRAYTEXT
      TColor($00C2CDD4),     // COLOR_3DDKSHADOW
      TColor($0064C8C0),     // COLOR_HOTLIGHT
      TColor($00000000),     // COLOR_INFOTEXT
      TColor($00E0EDEE)      // COLOR_INFOBK
    ),
    (
      TColor($00C8D0D4),     // COLOR_SCROLLBAR
      TColor($00E8E8E8),     // COLOR_MENU
      TColor($00000000),     // COLOR_MENUTEXT
      TColor($00967A55),     // COLOR_MENUHILIGHT
      TColor($00E8E8E8),     // COLOR_MENUBAR
      TColor($00FFFFFF),     // COLOR_WINDOW
      TColor($00000000),     // COLOR_WINDOWFRAME
      TColor($00000000),     // COLOR_WINDOWTEXT
      TColor($00C8D0D4),     // COLOR_ACTIVEBORDER
      TColor($00C8D0D4),     // COLOR_INACTIVEBORDER
      TColor($00808080),     // COLOR_APPWORKSPACE
      TColor($00967A55),     // COLOR_HIGHLIGHT
      TColor($00FFFFFF),     // COLOR_HIGHLIGHTTEXT
      TColor($00E8E8E8),     // COLOR_BTNFACE
      TColor($00C8C8C8),     // COLOR_BTNSHADOW
      TColor($00000000),     // COLOR_BTNTEXT
      TColor($00E8E8E8),     // COLOR_BTNHIGHLIGHT
      TColor($00EBEBEB),     // COLOR_3DLIGHT
      TColor($00C8C8C8),     // COLOR_GRAYTEXT
      TColor($00A0A0A0),     // COLOR_3DDKSHADOW
      TColor($00B7B7B7),     // COLOR_HOTLIGHT
      TColor($00000000),     // COLOR_INFOTEXT
      TColor($00E8E8E8)      // COLOR_INFOBK
    ),
    (
      TColor($00C8D0D4),     // COLOR_SCROLLBAR
      TColor($00FFFFFF),     // COLOR_MENU
      TColor($004B4B4B),     // COLOR_MENUTEXT
      TColor($00EA9A3B),     // COLOR_MENUHILIGHT
      TColor($00F3EFEF),     // COLOR_MENUBAR
      TColor($00FFFFFF),     // COLOR_WINDOW
      TColor($00000000),     // COLOR_WINDOWFRAME
      TColor($00000000),     // COLOR_WINDOWTEXT
      TColor($00C8D0D4),     // COLOR_ACTIVEBORDER
      TColor($00C8D0D4),     // COLOR_INACTIVEBORDER
      TColor($00808080),     // COLOR_APPWORKSPACE
      TColor($00DD7E26),     // COLOR_HIGHLIGHT
      TColor($00FFFFFF),     // COLOR_HIGHLIGHTTEXT
      TColor($00F3EFEF),     // COLOR_BTNFACE
      TColor($00ACA899),     // COLOR_BTNSHADOW
      TColor($00000000),     // COLOR_BTNTEXT
      TColor($00FFFFFF),     // COLOR_BTNHIGHLIGHT
      TColor($00E2EFF1),     // COLOR_3DLIGHT
      TColor($0099A8AC),     // COLOR_GRAYTEXT
      TColor($00C8C5C3),     // COLOR_3DDKSHADOW
      TColor($00800000),     // COLOR_HOTLIGHT
      TColor($00000000),     // COLOR_INFOTEXT
      TColor($00E8E8E8)      // COLOR_INFOBK
    ),
    (
      TColor($00C8D0D4),     // COLOR_SCROLLBAR
      TColor($00DCDCDC),     // COLOR_MENU
      TColor($00000000),     // COLOR_MENUTEXT
      TColor($00926836),     // COLOR_MENUHILIGHT
      TColor($00DCDCDC),     // COLOR_MENUBAR
      TColor($00FFFFFF),     // COLOR_WINDOW
      TColor($00000000),     // COLOR_WINDOWFRAME
      TColor($00000000),     // COLOR_WINDOWTEXT
      TColor($00C8D0D4),     // COLOR_ACTIVEBORDER
      TColor($00C8D0D4),     // COLOR_INACTIVEBORDER
      TColor($00808080),     // COLOR_APPWORKSPACE
      TColor($00926836),     // COLOR_HIGHLIGHT
      TColor($00FFFFFF),     // COLOR_HIGHLIGHTTEXT
      TColor($00DCDCDC),     // COLOR_BTNFACE
      TColor($00B4B4B4),     // COLOR_BTNSHADOW
      TColor($00000000),     // COLOR_BTNTEXT
      TColor($00DCDCDC),     // COLOR_BTNHIGHLIGHT
      TColor($00EBEBEB),     // COLOR_3DLIGHT
      TColor($00B4B4B4),     // COLOR_GRAYTEXT
      TColor($00A0A0A0),     // COLOR_3DDKSHADOW
      TColor($00B7B7B7),     // COLOR_HOTLIGHT
      TColor($00000000),     // COLOR_INFOTEXT
      TColor($00DCDCDC)      // COLOR_INFOBK
    ),
    (
      TColor($00D0D0D0),     // COLOR_SCROLLBAR
      TColor($00FFFFFF),     // COLOR_MENU
      TColor($00000000),     // COLOR_MENUTEXT
      TColor($009A785C),     // COLOR_MENUHILIGHT
      TColor($00FFFFFF),     // COLOR_MENUBAR
      TColor($00FFFFFF),     // COLOR_WINDOW
      TColor($00000000),     // COLOR_WINDOWFRAME
      TColor($00000000),     // COLOR_WINDOWTEXT
      TColor($00D0D0D0),     // COLOR_ACTIVEBORDER
      TColor($00D0D0D0),     // COLOR_INACTIVEBORDER
      TColor($00808080),     // COLOR_APPWORKSPACE
      TColor($0099705E),     // COLOR_HIGHLIGHT
      TColor($00FFFFFF),     // COLOR_HIGHLIGHTTEXT
      TColor($00FFFFFF),     // COLOR_BTNFACE
      TColor($00C2CDD4),     // COLOR_BTNSHADOW
      TColor($00000000),     // COLOR_BTNTEXT
      TColor($00FFFFFF),     // COLOR_BTNHIGHLIGHT
      TColor($00E2EFF1),     // COLOR_3DLIGHT
      TColor($0099A8AC),     // COLOR_GRAYTEXT
      TColor($00C2CDD4),     // COLOR_3DDKSHADOW
      TColor($00800000),     // COLOR_HOTLIGHT
      TColor($00000000),     // COLOR_INFOTEXT
      TColor($00FFFFFF)      // COLOR_INFOBK
    )
  );




  function SelectedThemeName: string;
  function GetThemeIndex(ThemeName: String): Integer;
  function GetColor(Color: TRGBColor): TColor;
  function TColorToRGB(Color: TColor): TRGBColor;
  function ColorThemeExists(ThemeName: string) : boolean;
  function ValidColorThemeName(NewName: string) : boolean;
  function ChangeBrightness(Color: TColor; Shift: Integer): TColor;
  function ChangeBlueColor(Color: TColor; Shift: Integer): TColor;
  function ChangeRedColor(Color: TColor; Shift: Integer): TColor;
  function ChangeGreenColor(Color: TColor; Shift: Integer): TColor;
  function GetSelectionColor(Color: TColor): TColor;
  function GetHighlightColor(Color: TColor): TColor;
  procedure SetupColorBars(Theme: TColorTheme);
  procedure AddColorTheme(Theme: TColorTheme);
  procedure SetColorTheme(Theme: TColorTheme);
  procedure SetColorThemeByName(ThemeName: string);
  procedure InitColorThemes;
  procedure FillColorThemesList;
  procedure UpdateCurrentTheme;
  procedure SaveColorTheme(FileName, ThemeName: String);
  procedure LoadColorTheme(FileName: String);
  procedure CloneColorTheme;
  function GetColorTheme(ThemeName: String): TColorTheme;
  function GetCurrentColorTheme: TColorTheme;
  procedure RenameSelectedTheme;
  procedure DeleteSelectedTheme;
  function AllUserThemesToStr: TThemesArray;
  function LoadColorThemeFromStr(Str: String): TColorTheme;
  procedure SaveSystemColors;
  procedure RestoreSystemColors;
  procedure SetWindowColors(ThemeIndex: Integer);

var
  VTColorThemes: array of TColorTheme;
  SystemColors: array[0..22] of LongInt;

implementation

uses main, options;

function GetColor(Color: TRGBColor): TColor;
var bgr: string;
begin
  bgr := '$00'+Color[5]+Color[6]+Color[3]+Color[4]+Color[1]+Color[2];
  Result := StringToColor(bgr);
end;

function TColorToRGB(Color: TColor): TRGBColor;
begin
  Result := Format('%.2x%.2x%.2x', [byte(color), byte(color shr 8), byte(color shr 16)]);
end;

function ChangeBrightness(Color: TColor; Shift: Integer): TColor;
var
  R, G, B: Integer;
begin
  R := byte(Color);
  G := byte(Color shr 8);
  B := byte(Color shr 16);
  R := R + Shift;
  G := G + Shift;
  B := B + Shift;
  if R < 0 then
    R := 0;
  if R > 255 then
    R := 255;
  if G < 0 then
    G := 0;
  if G > 255 then
    G := 255;
  if B < 0 then
    B := 0;
  if B > 255 then
    B := 255;
  Result := StringToColor(Format('$00%.2x%.2x%.2x', [B, G, R]));
end;

function ChangeBlueColor(Color: TColor; Shift: Integer): TColor;
var
  R, G, B: Integer;
begin
  R := byte(Color);
  G := byte(Color shr 8);
  B := byte(Color shr 16);
  B := B + Shift;
  if B < 0 then
    B := 0;
  if B > 255 then
    B := 255;
  Result := StringToColor(Format('$00%.2x%.2x%.2x', [B, G, R]));
end;

function ChangeRedColor(Color: TColor; Shift: Integer): TColor;
var
  R, G, B: Integer;
begin
  R := byte(Color);
  G := byte(Color shr 8);
  B := byte(Color shr 16);
  R := R + Shift;
  if R < 0 then
    R := 0;
  if R > 255 then
    R := 255;
  Result := StringToColor(Format('$00%.2x%.2x%.2x', [B, G, R]));
end;

function ChangeGreenColor(Color: TColor; Shift: Integer): TColor;
var
  R, G, B: Integer;
begin
  R := byte(Color);
  G := byte(Color shr 8);
  B := byte(Color shr 16);
  G := G + Shift;
  if G < 0 then
    G := 0;
  if G > 255 then
    G := 255;
  Result := StringToColor(Format('$00%.2x%.2x%.2x', [B, G, R]));
end;

function GetSelectionColor(Color: TColor): TColor;
var
  R, G, B, R1, G1, B1: Integer;
begin

  R := byte(Color);
  G := byte(Color shr 8);
  B := byte(Color shr 16);

  R1 := byte(Color);
  G1 := byte(Color shr 8);
  B1 := byte(Color shr 16);

  R := R - 30;
  G := G - 30;
  B := B - 30;

  if R < 40 then
    R := 40;
  if G < 40 then
    G := 40;
  if B < 40 then
    B := 40;

  if (B1 < R1) and (B1 < G1) then
    B := B + 30
  else
  if (R1 < B1) and (R1 < G1) then
    R := R + 30
  else
  if (G1 < B1) and (G1 < R1) then
    G := G + 30
  else
    B := B + 30;

  if R > 200 then
    R := 200;
  if G > 200 then
    G := 200;
  if B > 200 then
    B := 200;

  Result := StringToColor(Format('$00%.2x%.2x%.2x', [B, G, R]));
end;


function GetHighlightColor(Color: TColor): TColor;
var
  R, G, B, R1, G1, B1: Integer;
begin

  R := byte(Color);
  G := byte(Color shr 8);
  B := byte(Color shr 16);

  R1 := byte(Color);
  G1 := byte(Color shr 8);
  B1 := byte(Color shr 16);

  R := R + 15;
  G := G + 15;
  B := B + 15;

  if R > 200 then
    R := 200;
  if G > 200 then
    G := 200;
  if B > 200 then
    B := 200;

  if (B1 < R1) and (B1 < G1) then
    B := B + 10
  else
  if (R1 < B1) and (R1 < G1) then
    R := R + 10
  else
  if (G1 < B1) and (G1 < R1) then
    G := G + 10;

  if R >= 255 then
    R := 200;
  if G >= 255 then
    G := 200;
  if B >= 255 then
    B := 200;

  if R < 40 then
    R := 40;
  if G < 40 then
    G := 40;
  if B < 40 then
    B := 40;

  Result := StringToColor(Format('$00%.2x%.2x%.2x', [B, G, R]));
end;



procedure SetupColorBars(Theme: TColorTheme);
begin
  if not Assigned(Form1) then
    Exit;

  Form1.ColBackground.Brush.Color := GetColor(Theme.Background);
  Form1.ColSelLineBackground.Brush.Color := GetColor(Theme.SelLineBackground);
  Form1.ColHighlBackground.Brush.Color := GetColor(Theme.HighlBackground);
  Form1.ColOutBackground.Brush.Color := GetColor(Theme.OutBackground);
  Form1.ColOutHlBackground.Brush.Color := GetColor(Theme.OutHlBackground);
  Form1.ColText.Brush.Color := GetColor(Theme.Text);
  Form1.ColSelLineText.Brush.Color := GetColor(Theme.SelLineText);
  Form1.ColHighlText.Brush.Color := GetColor(Theme.HighlText);
  Form1.ColOutText.Brush.Color := GetColor(Theme.OutText);
  Form1.ColLineNum.Brush.Color := GetColor(Theme.LineNum);
  Form1.ColSelLineNum.Brush.Color := GetColor(Theme.SelLineNum);
  Form1.ColEnvelope.Brush.Color := GetColor(Theme.Envelope);
  Form1.ColSelEnvelope.Brush.Color := GetColor(Theme.SelEnvelope);
  Form1.ColNoise.Brush.Color := GetColor(Theme.Noise);
  Form1.ColSelNoise.Brush.Color := GetColor(Theme.SelNoise);
  Form1.ColNote.Brush.Color := GetColor(Theme.Note);
  Form1.ColSelNote.Brush.Color := GetColor(Theme.SelNote);
  Form1.ColNoteParams.Brush.Color := GetColor(Theme.NoteParams);
  Form1.ColSelNoteParams.Brush.Color := GetColor(Theme.SelNoteParams);
  Form1.ColNoteCommands.Brush.Color := GetColor(Theme.NoteCommands);
  Form1.ColSelNoteCommands.Brush.Color := GetColor(Theme.SelNoteCommands);
  Form1.ColSeparators.Brush.Color := GetColor(Theme.Separators);
  Form1.ColOutSeparators.Brush.Color := GetColor(Theme.OutSeparators);
  Form1.ColSamOrnBackground.Brush.Color := GetColor(ColorTheme.SamOrnBackground);
  Form1.ColSamOrnSelBackground.Brush.Color := GetColor(ColorTheme.SamOrnSelBackground);
  Form1.ColSamOrnText.Brush.Color := GetColor(ColorTheme.SamOrnText);
  Form1.ColSamOrnSelText.Brush.Color := GetColor(ColorTheme.SamOrnSelText);
  Form1.ColSamOrnLineNum.Brush.Color := GetColor(ColorTheme.SamOrnLineNum);
  Form1.ColSamOrnSelLineNum.Brush.Color := GetColor(ColorTheme.SamOrnSelLineNum);
  Form1.ColSamNoise.Brush.Color := GetColor(ColorTheme.SamNoise);
  Form1.ColSamSelNoise.Brush.Color := GetColor(ColorTheme.SamSelNoise);
  Form1.ColSamOrnSeparators.Brush.Color := GetColor(ColorTheme.SamOrnSeparators);
  Form1.ColSamTone.Brush.Color := GetColor(ColorTheme.SamTone);
  Form1.ColSamSelTone.Brush.Color := GetColor(ColorTheme.SamSelTone);
  Form1.ColFullScreenBackground.Brush.Color := GetColor(ColorTheme.FullScreenBackground);

end;

function SelectedThemeName: string;
var i: Integer;
begin
  Result := '';
  for i := 0 to Form1.ColorThemesList.Count-1 do
    if Form1.ColorThemesList.Selected[i] then
    begin
      Result := Form1.ColorThemesList.Items[i];
      Exit;
    end;
end;


function GetThemeIndex(ThemeName: String): Integer;
var i: Integer;
begin
  Result := -1;
  for i := 0 to Length(VTColorThemes)-1 do
    if VTColorThemes[i].Name = ThemeName then
    begin
      Result := i;
      break;
    end;
end;


function ColorThemeExists(ThemeName: string) : boolean;
var i: Integer;
begin

  for i := 0 to High(VTColorThemes) do
    if VTColorThemes[i].Name = ThemeName then
    begin
      Result := True;
      Exit;
    end;

  Result := False;

end;

function ValidColorThemeName(NewName: string) : boolean;
begin

  NewName := Trim(NewName);

  if NewName = '' then
  begin
    Result := False;
    Exit;
  end;

  Result := not ColorThemeExists(NewName);

end;


procedure AddColorTheme(Theme: TColorTheme);
var i: Integer;
begin

  // Increase themes array length
  SetLength(VTColorThemes, Length(VTColorThemes)+1);

  // Shift themes
  for i := Length(VTColorThemes)-1 downto 1 do
    VTColorThemes[i] := VTColorThemes[i-1];

  // Insert new theme at first position
  VTColorThemes[0] := Theme;
  ColorTheme := Theme;
  MainForm.PrepareColors;
  FillColorThemesList;

end;


procedure SetColorTheme(Theme: TColorTheme);
var
  i: Integer;
  Redraw: Boolean;
begin
  Redraw := False;
  if ColorThemeName <> Theme.Name then
    Redraw := True;

  ColorThemeName := Theme.Name;
  ColorTheme := Theme;
  MainForm.PrepareColors;
  if Assigned(Form1) then
  begin
    i := GetThemeIndex(Theme.Name);
    Form1.ColorThemesList.Selected[i] := True;
    SetupColorBars(Theme);
  end;

  if Redraw and not SyncVTInstanses then
    MainForm.RedrawChilds;
end;


procedure SetColorThemeByName(ThemeName: string);
var Theme: TColorTheme;
begin
  Theme := GetColorTheme(ThemeName);
  SetColorTheme(Theme);
end;



procedure InitColorThemes;
var
  i: Integer;
begin

  if Length(VTColorThemes) = 0 then
  begin
    SetLength(VTColorThemes, Length(DefaultColorThemes));
    for i := Low(DefaultColorThemes) to High(DefaultColorThemes) do
      VTColorThemes[i] := DefaultColorThemes[i];
  end;

  FillColorThemesList;

  if GetThemeIndex(ColorThemeName) = -1 then
    SetColorTheme(VTColorThemes[0])
  else
    SetColorThemeByName(ColorThemeName);

end;


procedure FillColorThemesList;
var
  I : Integer;
begin
  if not Assigned(Form1) then Exit;
  Form1.ColorThemesList.Clear;

  for I := 0 to Length(VTColorThemes) - 1 do
    begin
     Form1.ColorThemesList.Items.Add(VTColorThemes[I].Name);
      if VTColorThemes[I].Name = ColorThemeName then
        Form1.ColorThemesList.Selected[I] := True;
    end;


  I := GetThemeIndex(ColorThemeName);
  if I = -1 then
  begin
    SetColorTheme(VTColorThemes[0]);
  end
  else
    SetColorTheme(VTColorThemes[I]);

  //Form1.BtnRenameTheme.Enabled := Form1.BtnSaveTheme.Enabled;
  Form1.BtnDelTheme.Enabled := Length(VTColorThemes) > 1;

end;

procedure UpdateCurrentTheme;
var
  Theme: TColorTheme;
  i: Integer;
begin
  Theme := GetCurrentColorTheme;
  i := GetThemeIndex(Theme.Name);
  VTColorThemes[i] := ColorTheme;
end;


procedure SaveColorTheme(FileName, ThemeName: String);
var
  ini: TextFile;
  Theme: TColorTheme;
begin
  Theme := GetColorTheme(ThemeName);
  AssignFile(ini, FileName);
  Rewrite(ini);
  try
    Writeln(ini, '['+ ThemeINIKey +']');
    Writeln(ini, 'Name=' + ThemeName);
    Writeln(ini, 'Background=' + Theme.Background);
    Writeln(ini, 'SelLineBackground=' + Theme.SelLineBackground);
    Writeln(ini, 'HighlBackground=' + Theme.HighlBackground);
    Writeln(ini, 'OutBackground=' + Theme.OutBackground);
    Writeln(ini, 'OutHlBackground=' + Theme.OutHlBackground);
    Writeln(ini, 'Text=' + Theme.Text);
    Writeln(ini, 'SelLineText=' + Theme.SelLineText);
    Writeln(ini, 'HighlText=' + Theme.HighlText);
    Writeln(ini, 'OutText=' + Theme.OutText);
    Writeln(ini, 'LineNum=' + Theme.LineNum);
    Writeln(ini, 'SelLineNum=' + Theme.SelLineNum);
    Writeln(ini, 'Envelope=' + Theme.Envelope);
    Writeln(ini, 'SelEnvelope=' + Theme.SelEnvelope);
    Writeln(ini, 'Noise=' + Theme.Noise);
    Writeln(ini, 'SelNoise=' + Theme.SelNoise);
    Writeln(ini, 'Note=' + Theme.Note);
    Writeln(ini, 'SelNote=' + Theme.SelNote);
    Writeln(ini, 'NoteParams=' + Theme.NoteParams);
    Writeln(ini, 'SelNoteParams=' + Theme.SelNoteParams);
    Writeln(ini, 'NoteCommands=' + Theme.NoteCommands);
    Writeln(ini, 'SelNoteCommands=' + Theme.SelNoteCommands);
    Writeln(ini, 'Separators=' + Theme.Separators);
    Writeln(ini, 'OutSeparators=' + Theme.OutSeparators);
    Writeln(ini, 'SamOrnBackground=' + Theme.SamOrnBackground);
    Writeln(ini, 'SamOrnSelBackground=' + Theme.SamOrnSelBackground);
    Writeln(ini, 'SamOrnText=' + Theme.SamOrnText);
    Writeln(ini, 'SamOrnSelText=' + Theme.SamOrnSelText);
    Writeln(ini, 'SamOrnLineNum=' + Theme.SamOrnLineNum);
    Writeln(ini, 'SamOrnSelLineNum=' + Theme.SamOrnSelLineNum);
    Writeln(ini, 'SamNoise=' + Theme.SamNoise);
    Writeln(ini, 'SamSelNoise=' + Theme.SamSelNoise);
    Writeln(ini, 'SamOrnSeparators=' + Theme.SamOrnSeparators);
    Writeln(ini, 'SamTone=' + Theme.SamTone);
    Writeln(ini, 'SamSelTone=' + Theme.SamSelTone);
    Writeln(ini, 'FullScreenBackground=' + Theme.FullScreenBackground);
  finally
    CloseFile(ini);
  end;
end;


procedure LoadColorTheme(FileName: String);
var
  ini: TIniFile;
  Theme: TColorTheme;
  i: Integer;
  NewName: String;
  
begin
  ini := TIniFile.Create(FileName);
  try
    Theme.Name := ini.ReadString(ThemeINIKey,  'Name', ColorThemeName);
    Theme.Background := ini.ReadString(ThemeINIKey,  'Background', TColorToRGB(CBackground));
    Theme.SelLineBackground := ini.ReadString(ThemeINIKey,  'SelLineBackground', TColorToRGB(CSelLineBackground));
    Theme.HighlBackground := ini.ReadString(ThemeINIKey,  'HighlBackground', TColorToRGB(CHighlBackground));
    Theme.OutBackground := ini.ReadString(ThemeINIKey,  'OutBackground', TColorToRGB(COutBackground));
    Theme.OutHlBackground := ini.ReadString(ThemeINIKey,  'OutHlBackground', TColorToRGB(COutHlBackground));
    Theme.Text := ini.ReadString(ThemeINIKey,  'Text', TColorToRGB(CText));
    Theme.SelLineText := ini.ReadString(ThemeINIKey,  'SelLineText', TColorToRGB(CSelLineText));
    Theme.HighlText := ini.ReadString(ThemeINIKey,  'HighlText', TColorToRGB(CHighlText));
    Theme.OutText := ini.ReadString(ThemeINIKey,  'OutText', TColorToRGB(COutText));
    Theme.LineNum := ini.ReadString(ThemeINIKey,  'LineNum', TColorToRGB(CLineNum));
    Theme.SelLineNum := ini.ReadString(ThemeINIKey,  'SelLineNum', TColorToRGB(CSelLineNum));
    Theme.Envelope := ini.ReadString(ThemeINIKey,  'Envelope', TColorToRGB(CEnvelope));
    Theme.SelEnvelope := ini.ReadString(ThemeINIKey,  'SelEnvelope', TColorToRGB(CSelEnvelope));
    Theme.Noise := ini.ReadString(ThemeINIKey,  'Noise', TColorToRGB(CNoise));
    Theme.SelNoise := ini.ReadString(ThemeINIKey,  'SelNoise', TColorToRGB(CSelNoise));
    Theme.Note := ini.ReadString(ThemeINIKey,  'Note', TColorToRGB(CNote));
    Theme.SelNote := ini.ReadString(ThemeINIKey,  'SelNote', TColorToRGB(CSelNote));
    Theme.NoteParams := ini.ReadString(ThemeINIKey,  'NoteParams', TColorToRGB(CNoteParams));
    Theme.SelNoteParams := ini.ReadString(ThemeINIKey,  'SelNoteParams', TColorToRGB(CSelNoteParams));
    Theme.NoteCommands := ini.ReadString(ThemeINIKey,  'NoteCommands', TColorToRGB(CNoteCommands));
    Theme.SelNoteCommands := ini.ReadString(ThemeINIKey,  'SelNoteCommands', TColorToRGB(CSelNoteCommands));
    Theme.Separators := ini.ReadString(ThemeINIKey,  'Separators', TColorToRGB(CSeparators));
    Theme.OutSeparators := ini.ReadString(ThemeINIKey,  'OutSeparators', TColorToRGB(COutSeparators));
    Theme.SamOrnBackground := ini.ReadString(ThemeINIKey,  'SamOrnBackground', TColorToRGB(CSamOrnBackground));
    Theme.SamOrnSelBackground := ini.ReadString(ThemeINIKey,  'SamOrnSelBackground', TColorToRGB(CSamOrnSelBackground));
    Theme.SamOrnText := ini.ReadString(ThemeINIKey,  'SamOrnText', TColorToRGB(CSamOrnText));
    Theme.SamOrnSelText := ini.ReadString(ThemeINIKey,  'SamOrnSelText', TColorToRGB(CSamOrnSelText));
    Theme.SamOrnLineNum := ini.ReadString(ThemeINIKey,  'SamOrnLineNum', TColorToRGB(CSamOrnLineNum));
    Theme.SamOrnSelLineNum := ini.ReadString(ThemeINIKey,  'SamOrnSelLineNum', TColorToRGB(CSamOrnSelLineNum));
    Theme.SamNoise := ini.ReadString(ThemeINIKey,  'SamNoise', TColorToRGB(CSamNoise));
    Theme.SamSelNoise := ini.ReadString(ThemeINIKey,  'SamSelNoise', TColorToRGB(CSamSelNoise));
    Theme.SamOrnSeparators := ini.ReadString(ThemeINIKey,  'SamOrnSeparators', TColorToRGB(CSamOrnSeparators));
    Theme.SamTone := ini.ReadString(ThemeINIKey,  'SamTone', TColorToRGB(CSamTone));
    Theme.SamSelTone := ini.ReadString(ThemeINIKey,  'SamSelTone', TColorToRGB(CSamSelTone));
    Theme.FullScreenBackground := ini.ReadString(ThemeINIKey,  'FullScreenBackground', TColorToRGB(CFullScreenBackground));
    if ColorThemeExists(Theme.Name) then
    begin
      i := 1;
      repeat
        NewName := Theme.Name + ' ' + IntToStr(i);
        Inc(i);
      until not ColorThemeExists(NewName);
      Theme.Name := NewName;
    end;

    AddColorTheme(Theme);
    SetColorTheme(Theme);
    
  finally
    ini.Free;
  end;
  
end;


procedure CloneColorTheme;
var
  NewName: String;
  Theme: TColorTheme;
begin
  Theme := GetCurrentColorTheme;
  repeat
    if not InputQuery('Vortex Tracker II', 'Enter new theme name', NewName) then
      Exit;
  until ValidColorThemeName(NewName);
  Theme.Name := NewName;

  AddColorTheme(Theme);
  SetColorTheme(Theme);
end;



function GetColorTheme(ThemeName: String): TColorTheme;
var
  i: Integer;
  ok: Boolean;
begin
  ok := False;
  for i := 0 to Length(VTColorThemes)-1 do
    if VTColorThemes[i].Name = ThemeName then
    begin
      Result := VTColorThemes[i];
      ok := True;
      Break;
    end;

  if not ok then
    Result := GetColorTheme('Default');

end;


function GetCurrentColorTheme: TColorTheme;
var
  ThemeName: String;
  i: Integer;
begin
  ThemeName := SelectedThemeName;
  for i := 0 to Form1.ColorThemesList.Count-1 do
    if Form1.ColorThemesList.Items[i] = ThemeName then
      Result := VTColorThemes[i];
end;

procedure RenameSelectedTheme;
var
  i: Integer;
  NewName: String;
begin
  repeat
    if not InputQuery('Vortex Tracker II', 'Enter new name', NewName) then
      Exit;
  until ValidColorThemeName(NewName);

  i := GetThemeIndex(SelectedThemeName);
  VTColorThemes[i].Name := NewName;
  ColorThemeName := NewName;

  FillColorThemesList;

end;


procedure DeleteSelectedTheme;
var
  Theme: TColorTheme;
  i: Integer;
begin
  Theme := GetCurrentColorTheme;

  if MessageDlg('Are you sure?', mtWarning, mbOKCancel, 0) = mrCancel then
    exit;

  // Shift themes to right
  for i := GetThemeIndex(Theme.Name) to Length(VTColorThemes)-2 do
    VTColorThemes[i] := VTColorThemes[i+1];

  // Decrease themes array
  SetLength(VTColorThemes, Length(VTColorThemes)-1);

  SetColorTheme(VTColorThemes[0]);
  FillColorThemesList;

end;


function AllUserThemesToStr: TThemesArray;
var
  i: Integer;
  s: string;
begin
  SetLength(Result, Length(VTColorThemes));
  for i := Low(VTColorThemes) to High(VTColorThemes) do
    begin
      s := VTColorThemes[i].Name + Chr(180);
      s := s + VTColorThemes[i].Background + ',';
      s := s + VTColorThemes[i].SelLineBackground + ',';
      s := s + VTColorThemes[i].HighlBackground + ',';
      s := s + VTColorThemes[i].OutBackground + ',';
      s := s + VTColorThemes[i].OutHlBackground + ',';
      s := s + VTColorThemes[i].Text + ',';
      s := s + VTColorThemes[i].SelLineText + ',';
      s := s + VTColorThemes[i].HighlText + ',';
      s := s + VTColorThemes[i].OutText + ',';
      s := s + VTColorThemes[i].LineNum + ',';
      s := s + VTColorThemes[i].SelLineNum + ',';
      s := s + VTColorThemes[i].Envelope + ',';
      s := s + VTColorThemes[i].SelEnvelope + ',';
      s := s + VTColorThemes[i].Noise + ',';
      s := s + VTColorThemes[i].SelNoise + ',';
      s := s + VTColorThemes[i].Note + ',';
      s := s + VTColorThemes[i].SelNote + ',';
      s := s + VTColorThemes[i].NoteParams + ',';
      s := s + VTColorThemes[i].SelNoteParams + ',';
      s := s + VTColorThemes[i].NoteCommands + ',';
      s := s + VTColorThemes[i].SelNoteCommands + ',';
      s := s + VTColorThemes[i].Separators + ',';
      s := s + VTColorThemes[i].OutSeparators + ',';
      s := s + VTColorThemes[i].SamOrnBackground + ',';
      s := s + VTColorThemes[i].SamOrnSelBackground + ',';
      s := s + VTColorThemes[i].SamOrnText + ',';
      s := s + VTColorThemes[i].SamOrnSelText + ',';
      s := s + VTColorThemes[i].SamOrnLineNum + ',';
      s := s + VTColorThemes[i].SamOrnSelLineNum + ',';
      s := s + VTColorThemes[i].SamNoise + ',';
      s := s + VTColorThemes[i].SamSelNoise + ',';
      s := s + VTColorThemes[i].SamOrnSeparators + ',';
      s := s + VTColorThemes[i].SamTone + ',';
      s := s + VTColorThemes[i].SamSelTone + ',';
      s := s + VTColorThemes[i].FullScreenBackground;
      Result[i] := s;
    end;
end;

function LoadColorThemeFromStr(Str: String): TColorTheme;
var
  Part, Color: TStrings;

begin

  if Trim(Str) = '' then Exit;
  try
    Part  := Split(Chr(180), Str);
    if Part.Count <> 2 then Exit;
    Color := Split(',', Part[1]);
    if Color.Count <> 35 then Exit;
    if Trim(Part[0]) = '' then Exit;

    Result.Name := Part[0];
    Result.Background := Color[0];
    Result.SelLineBackground := Color[1];
    Result.HighlBackground := Color[2];
    Result.OutBackground := Color[3];
    Result.OutHlBackground := Color[4];
    Result.Text := Color[5];
    Result.SelLineText := Color[6];
    Result.HighlText := Color[7];
    Result.OutText := Color[8];
    Result.LineNum := Color[9];
    Result.SelLineNum := Color[10];
    Result.Envelope := Color[11];
    Result.SelEnvelope := Color[12];
    Result.Noise := Color[13];
    Result.SelNoise := Color[14];
    Result.Note := Color[15];
    Result.SelNote := Color[16];
    Result.NoteParams := Color[17];
    Result.SelNoteParams := Color[18];
    Result.NoteCommands := Color[19];
    Result.SelNoteCommands := Color[20];
    Result.Separators := Color[21];
    Result.OutSeparators := Color[22];
    Result.SamOrnBackground := Color[23];
    Result.SamOrnSelBackground := Color[24];
    Result.SamOrnText := Color[25];
    Result.SamOrnSelText := Color[26];
    Result.SamOrnLineNum := Color[27];
    Result.SamOrnSelLineNum := Color[28];
    Result.SamNoise := Color[29];
    Result.SamSelNoise := Color[30];
    Result.SamOrnSeparators := Color[31];
    Result.SamTone := Color[32];
    Result.SamSelTone := Color[33];
    Result.FullScreenBackground := Color[34];
  finally
    FreeAndNil(Part);
    FreeAndNil(Color);
  end;

end;

procedure SaveSystemColors;
var i: Integer;
begin

  for i := 0 to High(WinColors) do
  begin
    SystemColors[i] := GetSysColor(WinColors[i]);
    WinColorThemes[0][i] := SystemColors[i];
  end;

end;


procedure RestoreSystemColors;
begin
  SetSysColors(Length(SystemColors), WinColors, SystemColors);
end;


procedure SetWindowColors(ThemeIndex: Integer);

begin
  SetSysColors(Length(SystemColors), WinColors, WinColorThemes[ThemeIndex]);
end;


end.
