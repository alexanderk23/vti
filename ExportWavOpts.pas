unit ExportWavOpts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, AY;

type
  TExportOptions = class(TForm)
    Chip: TRadioGroup;
    SampleRate: TRadioGroup;
    GroupBox1: TGroupBox;
    ExportNumLoops: TLabel;
    LoopRepeats: TUpDown;
    LpRepeat: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    function GetSampleRate: Integer;
    function GetChip: ChTypes;
    function GetRepeats: Integer;
  end;

var
  ExportOptions: TExportOptions;


implementation

uses main;

{$R *.dfm}

procedure TExportOptions.FormCreate(Sender: TObject);
begin
  SampleRate.ItemIndex := MainForm.ExportSampleRate;
  Chip.ItemIndex       := MainForm.ExportChip;
  LoopRepeats.Position := MainForm.ExportRepeats;
end;


function TExportOptions.GetSampleRate: Integer;
begin
  Result := 44100;
  case MainForm.ExportSampleRate of
    0: Result := 22050;
    1: Result := 44100;
    2: Result := 48000;
    3: Result := 88200;
    4: Result := 96000;
  end;
end;

function TExportOptions.GetChip: ChTypes;
begin
  Result := YM_Chip;
  case MainForm.ExportChip of
    0: Result := AY_Chip;
    1: Result := YM_Chip;
  end;
end;


function TExportOptions.GetRepeats: Integer;
begin
  Result := MainForm.ExportRepeats;
end;


procedure TExportOptions.Button1Click(Sender: TObject);
begin
  MainForm.ExportSampleRate := SampleRate.ItemIndex;
  MainForm.ExportChip       := Chip.ItemIndex;
  MainForm.ExportRepeats    := LoopRepeats.Position;
  ModalResult := mrOk;
end;

procedure TExportOptions.Button2Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
