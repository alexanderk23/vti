{
This is part of Vortex Tracker II project

(c)2000-2009 S.V.Bulba
Author: Sergey Bulba, vorobey@mail.khstu.ru
Support page: http://bulba.untergrund.net/

Version 2.0 and later
(c)2017-2018 Ivan Pirog, ivan.pirog@gmail.com
}

unit WaveOutAPI;

interface

uses Windows, Messages, Dialogs, MMSystem, SysUtils, Forms, trfuncs, Classes,
     ExportWav;

type
//Digital sound data buffer
  TWaveOutBuffer = packed array of byte;

  TWaveHeader = record
    idRiff: array [0..3] of AnsiChar;
    RiffLen: longint;
    idWave: array [0..3] of AnsiChar;
    idFmt: array [0..3] of AnsiChar;
    InfoLen: longint;
    WaveType: smallint;
    Ch: smallint;
    Freq: longint;
    BytesPerSec: longint;
    align: smallint;
    Bits: smallint;
    idData: array [0..3] of char;
    DataLen: longint;
  end;


const
  NumberOfBuffersDef = 3;
  BufLen_msDef = 100; //726;
  WODevice: DWORD = WAVE_MAPPER;

var
  NumberOfBuffers, BufferLength, BuffLen, BufLen_ms: integer;
  NOfTicks: DWORD;
  IsPlaying: boolean = False;
  Reseted: boolean = False;
  Interrupt_Freq, NumberOfChannels, SampleRate, SampleBit: integer;
  PlayingGrid: array of record
    M1, M2: integer;
  end;
  MkVisPos, VisPosMax, VisPoint, VisStep, VisTickMax: DWORD;
  ResetMutex: THandle;
  HWO: HWAVEOUT;
  waveOutBuffers: array of record
    Buf: TWaveOutBuffer;
    WH: WAVEHDR;
  end;
  LineReady: boolean;
  ExportStarted, ExportFinished: Boolean;
  ExportLoops: Integer;



procedure InitForAllTypes(All: boolean);
procedure StartWOThread;
procedure WOThreadFinalization;
procedure StopPlaying;
procedure WOCheck(Res: MMRESULT);
procedure ResetAYChipEmulation(chip: integer);
function WOThreadActive: boolean;
procedure ResetPlaying;
procedure UnresetPlaying;
procedure DisableControls;
procedure EnableControls;
procedure CreateWaveHeader(Length, SamplesPerSec: Integer;
  BitsPerSample, Channeles: Smallint; var WaveHeader: TWaveHeader);
procedure CreateWave(FileName: string);

implementation

uses AY, Childwin, Main, ExportWavOpts;

var
  WOEventH: THANDLE;
  WOThreadID: DWORD;
  WOThreadH: THANDLE = 0;
  WOCS: RTL_CRITICAL_SECTION;

  TSEventH: THANDLE;
  TSThreadID: DWORD;
  TSThreadH: THANDLE;

  AudioProblem: Boolean;

type
  EMultiMediaError = class(Exception);

procedure WOCheck(Res: MMRESULT);
var
  ErrMsg: array[0..255] of Char;
begin
  if Res <> 0 then
  begin
    EnterCriticalSection(WOCS);
    waveOutGetErrorText(Res, ErrMsg, SizeOf(ErrMsg));
    LeaveCriticalSection(WOCS);
    raise EMultiMediaError.Create(ErrMsg)
  end
end;

function TSThreadFunc(a: pointer): dword; stdcall;
var
  CurVisPos, t: DWORD;
  MMTIME1: MMTime;
begin
  while WaitForSingleObject(TSEventH, 0) <> WAIT_OBJECT_0 do
  begin
    if IsPlaying and WOThreadActive and (PlayMode in [PMPlayModule, PMPlayPattern]) then
    begin
      t := GetTickCount;
      MMTIME1.wType := TIME_SAMPLES;
      EnterCriticalSection(WOCS);
      waveOutGetPosition(HWO, @MMTIME1, sizeof(MMTIME1));
      LeaveCriticalSection(WOCS);
      if MMTIME1.sample <> 0 then //if woReseted then don't redraw
      begin
        CurVisPos := MMTIME1.sample mod VisTickMax div VisStep;
      //SendMessage directly call message function (no thread-safe}
      {SendMessage} PostMessage(MainForm.Handle, UM_REDRAWTRACKS,
          PlayingGrid[CurVisPos].M1, PlayingGrid[CurVisPos].M2);
      end;
      Inc(t, 20 - GetTickCount);
      if integer(t) < 0 then
        t := 0;
      Sleep(t)
    end
    else
      Sleep(20)
  end;
  Result := STILL_ACTIVE - 1;
end;

procedure StartTrackSlider;
begin
  TSEventH := CreateEvent(nil, False, False, nil);
  TSThreadH := CreateThread(nil, 0, @TSThreadFunc, nil, 0, TSThreadID)
end;

procedure SkipRedraw;
var
  msg: TMsg;
begin
  Sleep(0);
  if PlayMode in [PMPlayModule, PMPlayPattern] then
    PeekMessage(msg, MainForm.Handle, UM_REDRAWTRACKS, UM_REDRAWTRACKS, PM_REMOVE);
end;

procedure StopTrackSlider;
var
  ExCode: DWORD;
begin
  SetEvent(TSEventH);
  repeat
    if not GetExitCodeThread(TSThreadH, ExCode) then break;
    if ExCode = STILL_ACTIVE then SkipRedraw;
  until ExCode <> STILL_ACTIVE;
  CloseHandle(TSThreadH);
  CloseHandle(TSEventH);
end;

procedure WaitForWOThreadExit;
var
  ExCode: DWORD;
begin
  if WOThreadH = 0 then exit;
  repeat
    if not GetExitCodeThread(WOThreadH, ExCode) then break;
    if ExCode = STILL_ACTIVE then Sleep(0);
  until ExCode <> STILL_ACTIVE;
  CloseHandle(WOThreadH);
  WOThreadH := 0
end;

procedure StopPlaying;
var
  msg: TMsg;
begin
  if WOThreadActive then
  begin
    AudioProblem := False;
    IsPlaying := False;
    UnlimiteDelay := False;
    ResetPlaying;
    UnresetPlaying;
    SetEvent(WOEventH);
    WaitForWOThreadExit;
    while not PeekMessage(msg, MainForm.Handle,
      UM_FINALIZEWO, UM_FINALIZEWO, PM_REMOVE) do Sleep(0);
    WOThreadFinalization
  end
end;

function WOThreadFunc(a: pointer): dword; stdcall;

  function AllBuffersDone: boolean;
  var
    i: integer;
  begin
    Result := False;
    for i := 0 to NumberOfBuffers - 1 do
      if waveOutBuffers[i].WH.dwFlags and WHDR_DONE = 0 then exit;
    Result := True
  end;

var
  i, j, SampleSize: integer;
  mut: boolean;
begin
  SampleSize := (SampleBit div 8) * NumberOfChannels;
  mut := False;
  try
    repeat
      WaitForSingleObject(ResetMutex, INFINITE);
      mut := True;
      if not Real_End_All then
      begin
        for i := 0 to NumberOfBuffers - 1 do
          with waveOutBuffers[i] do
          begin
            if Reseted then break;
            if not IsPlaying then break;
            if WH.dwFlags and WHDR_DONE <> 0 then
            begin
              MakeBuffer(WH.lpdata);
              if Reseted then break;
              if not IsPlaying then break;
              if BuffLen = 0 then
              begin
                if AllBuffersDone then break
              end
              else
              begin
                WH.dwBufferLength := BuffLen * SampleSize;
                WH.dwFlags := WH.dwFlags and not WHDR_DONE;
                EnterCriticalSection(WOCS);
                try
                  WOCheck(waveOutWrite(HWO, @WH, sizeof(WAVEHDR)));
                except
                  // Audio device settings changed
                  LeaveCriticalSection(WOCS);
                  IsPlaying := False;
                  Reseted := False;
                  ReleaseMutex(ResetMutex);
                  AudioProblem := True;
                  WOCheck(waveOutClose(HWO));
                  Result := 0;
                  Exit;
                end;
                LeaveCriticalSection(WOCS);
              end
            end;
          end
      end;
      if Real_End_All and not Reseted and AllBuffersDone then break;
      mut := False;
      ReleaseMutex(ResetMutex);
      if not IsPlaying then break;
      j := WaitForSingleObject(WOEventH, BufLen_ms);
      if (j <> WAIT_OBJECT_0) and (j <> WAIT_TIMEOUT) then break
    until not IsPlaying
  finally
    if mut then
      ReleaseMutex(ResetMutex);
    PostMessage(MainForm.Handle, UM_FINALIZEWO, 0, 0);
    Result := STILL_ACTIVE - 1
  end
end;

procedure StartWOThread;
var
  pwfx: pcmwaveformat;
  i, bl: integer;
begin
  if WOThreadActive then exit;
  ExportStarted := False;
  AudioProblem := False;
  with pwfx.wf do
  begin
    wFormatTag := 1;
    nChannels := NumberOfChannels;
    nSamplesPerSec := SampleRate;
    nBlockAlign := (SampleBit div 8) * NumberOfChannels;
    nAvgBytesPerSec := SampleRate * pwfx.wf.nBlockAlign;
  end;
  pwfx.wBitsPerSample := SampleBit;
  try
    WOCheck(waveOutOpen(@HWO, WODevice, @pwfx, WOEventH, 0, CALLBACK_EVENT));
  except
    PostMessage(MainForm.Handle, UM_PLAYINGOFF, 0, 0);
    MessageDlg('Can''t play: audio device is busy or not available. Check sound settings.',
      mtWarning, [mbOK], 0);
    Exit;
  end;
  WaitForSingleObject(WOEventH, INFINITE);
  try
    bl := BufferLength * pwfx.wf.nBlockAlign;
    for i := 0 to NumberOfBuffers - 1 do
      with waveOutBuffers[i] do
      begin
        SetLength(Buf, bl);
        with WH do
        begin
          lpdata := @Buf[0];
          dwBufferLength := bl;
          dwFlags := 0;
          dwUser := 0;
          dwLoops := 0;
        end;
        WOCheck(waveOutPrepareHeader(HWO, @WH, sizeof(WAVEHDR)));
        WH.dwFlags := WH.dwFlags or WHDR_DONE;
      end
  except
    PostMessage(MainForm.Handle, UM_PLAYINGOFF, 0, 0);
    MessageDlg('Can''t play: audio device is busy or not available. Check sound settings.',
      mtWarning, [mbOK], 0);
    //Exit;
    WOCheck(waveOutClose(HWO));
    Exit;
  end;

  StartTrackSlider;

  IsPlaying := True;
  Reseted := False;
  WOThreadH := CreateThread(nil, 0, @WOThreadFunc, nil, 0, WOThreadID)
end;

procedure WOThreadFinalization;
var
  i: integer;
begin
  WaitForWOThreadExit;
  StopTrackSlider;

  try
    WOCheck(waveOutReset(HWO));
    for i := 0 to NumberOfBuffers - 1 do
      with waveOutBuffers[i] do
      begin
        while WH.dwFlags and WHDR_DONE = 0 do Sleep(0);
        if WH.dwFlags and WHDR_PREPARED <> 0 then
          WOCheck(waveOutUnprepareHeader(HWO, @WH, sizeof(WAVEHDR)));
        Buf := nil
      end;
    WOCheck(waveOutClose(HWO));
  except
    if AudioProblem then
    begin
      Application.MessageBox('Audio device settings was changed. Check audio options and try again.', 
        'Vortex Tracker', MB_OK + MB_ICONWARNING + MB_TOPMOST);
    end
    else
      ShowException(ExceptObject, ExceptAddr);
  end;

  IsPlaying := False;
  Reseted := False;
  AudioProblem := False;

end;

procedure ResetAYChipEmulation;
begin
  with SoundChip[chip] do
  begin
    FillChar(AYRegisters, 14, 0);
    SetEnvelopeRegister(0);
    First_Period := False;
    Ampl := 0;
    SetMixerRegister(0);
    SetAmplA(0);
    SetAmplB(0);
    SetAmplC(0);
    IntFlag := False;
    Number_Of_Tiks.Re := 0;
    Current_Tik := 0;
    Envelope_Counter.Re := 0;
    Ton_Counter_A.Re := 0;
    Ton_Counter_B.Re := 0;
    Ton_Counter_C.Re := 0;
    Noise_Counter.Re := 0;
    Ton_A := 0;
    Ton_B := 0;
    Ton_C := 0;
    Left_Chan := 0; Right_Chan := 0; Tick_Counter := 0;
    Tik.Re := Delay_In_Tiks;
    Noise.Seed := $FFFF;
    Noise.Val := 0
  end
end;

procedure InitForAllTypes;
var
  i: integer;
begin
  LineReady := False;
  MkVisPos := 0;
  VisPoint := 0;
  NOfTicks := 0;
  for i := 1 to NumberOfSoundChips do
  begin
    ResetAYChipEmulation(i);
    Real_End[i] := False;
  end;
  Real_End_All := False;
  if Optimization_For_Quality and IsFilt then
  begin
    FillChar(Filt_XL[0], (Filt_M + 1) * 4, 0);
    FillChar(Filt_XR[0], (Filt_M + 1) * 4, 0);
    Filt_I := 0;
  end;
  for i := NumberOfSoundChips downto 1 do
  begin
    Module_SetPointer(PlayingWindow[i].VTMP, i);
    InitTrackerParameters(All);
  end;
end;

function WOThreadActive;
var
  ExCode: DWORD;
begin
  Result := (WOThreadH <> 0) and
    GetExitCodeThread(WOThreadH, ExCode) and
    (ExCode = STILL_ACTIVE);
  if not Result then
    if WOThreadH <> 0 then
    begin
      CloseHandle(WOThreadH);
      WOThreadH := 0
    end
end;

procedure ResetPlaying;
var
  i: integer;
begin
  if Reseted then exit;
  Reseted := True;
  UnlimiteDelay := False;
  AudioProblem := False;
  WaitForSingleObject(ResetMutex, INFINITE);
  EnterCriticalSection(WOCS);
  WOCheck(waveOutReset(HWO));
  LeaveCriticalSection(WOCS);
  MkVisPos := 0;
  VisPoint := 0;
  NOfTicks := 0;
  for i := 0 to NumberOfBuffers - 1 do
    with waveOutBuffers[i] do
      while WH.dwFlags and WHDR_DONE = 0 do Sleep(0)
end;

procedure UnresetPlaying;
begin
  if Reseted then
  begin
    AudioProblem := False;
    SetEvent(WOEventH);
    Reseted := False;
    ReleaseMutex(ResetMutex)
  end
end;

procedure DisableControls;
begin
  MainForm.DisableControls(True);
  MainForm.ToolBar2.Enabled := False;
  MainForm.RFile1.Enabled   := False;
  MainForm.RFile2.Enabled   := False;
  MainForm.RFile3.Enabled   := False;
  MainForm.RFile4.Enabled   := False;
  MainForm.RFile5.Enabled   := False;
  MainForm.RFile6.Enabled   := False;
  MainForm.OpenDemo.Enabled := False;
  MainForm.Options1.Enabled := False;
  MainForm.Exports1.Enabled := False;
  MainForm.Togglesamples1.Enabled := False;
  MainForm.Tracksmanager1.Enabled := False;
  MainForm.Globaltransposition1.Enabled := False;
  PlayingWindow[1].PageControl1.Enabled := False;
  if PlayingWindow[2] <> nil then PlayingWindow[2].PageControl1.Enabled := False;
end;

procedure EnableControls;
begin
  MainForm.ToolBar2.Enabled := True;
  MainForm.RFile1.Enabled   := True;
  MainForm.RFile2.Enabled   := True;
  MainForm.RFile3.Enabled   := True;
  MainForm.RFile4.Enabled   := True;
  MainForm.RFile5.Enabled   := True;
  MainForm.RFile6.Enabled   := True;
  MainForm.OpenDemo.Enabled := True;
  MainForm.Options1.Enabled := True;
  MainForm.Exports1.Enabled := True;
  MainForm.Togglesamples1.Enabled := True;
  MainForm.Tracksmanager1.Enabled := True;
  MainForm.Globaltransposition1.Enabled := True;
  PlayingWindow[1].PageControl1.Enabled := True;
  if PlayingWindow[2] <> nil then PlayingWindow[2].PageControl1.Enabled := True;
  MainForm.RestoreControls;
end;


procedure CreateWaveHeader(Length, SamplesPerSec: Integer;
  BitsPerSample, Channeles: Smallint; var WaveHeader: TWaveHeader);
begin
  if (SamplesPerSec < 1) or (not BitsPerSample in [8, 16]) or (not Channeles in [1, 2])
    then raise Exception.Create('Wrong params');

  //len := SampleCount * BitsPerSample div 8 * Channeles;
  with WaveHeader do begin
    idRiff := 'RIFF';
    RiffLen := Length + 38;
    idWave := 'WAVE';
    idFmt := 'fmt ';
    InfoLen := 16;
    WaveType := 1;
    Ch := Channeles;
    Freq := SamplesPerSec;
    BytesPerSec := SamplesPerSec * BitsPerSample div 8 * Channeles;
    align := Channeles * BitsPerSample div 8;
    Bits := BitsPerSample;
    idData := 'data';
    DataLen := Length;
  end;
end;


procedure CreateWave(FileName: string);
var
  WaveHeader: TWaveHeader;
  AudioBufferSize, i: Integer;
  FileStream, TMPFileStream: TFileStream;
  Buf: TWaveOutBuffer;

  prevSampleRate, prevBitRate, prevNumChans: Integer;
  prevOptForQuality, prevLoopAllowed: Boolean;
  prevEmulatingChip: ChTypes;

  ExportModal: TExport;
  Chip1Position, Chip2Position: Integer;

  TMPFileName: string;

begin

  // Save chip & audio params
  prevSampleRate    := SampleRate;
  prevBitRate       := SampleBit;
  prevNumChans      := NumberOfChannels;
  prevEmulatingChip := Emulating_Chip;
  prevOptForQuality := Optimization_For_Quality;
  prevLoopAllowed   := LoopAllowed;


  PlayMode := PMPlayModule;

  if IsPlaying then
    StopPlaying;

  // Force set active tab to patterns tab
  PlayingWindow[1].PageControl1.ActivePageIndex := 0;
  if (PlayingWindow[2] <> nil) then PlayingWindow[2].PageControl1.ActivePageIndex := 0;

  DisableControls;

  // Set chip & audio params for export
  MainForm.SetEmulatingChip(ExportOptions.GetChip);
  Set_Optimization(True);
  SetSampleRate(ExportOptions.GetSampleRate);
  SetBitRate(16);
  SetNChans(2);
  InitForAllTypes(True);


  // Init pointer, position, delay
  for i := 1 to NumberOfSoundChips do
  begin
    Module_SetPointer(PlayingWindow[i].VTMP, i);
    Module_SetDelay(PlayingWindow[i].VTMP.Initial_Delay);
    Module_SetCurrentPosition(0);
  end;


  // Set loop repeats
  ExportLoops := ExportOptions.GetRepeats;
  LoopAllowed := ExportLoops > 0;

  // Show export modal
  ExportModal := TExport.Create(MainForm);
  ExportModal.ExportProgress.Position := 0;
  ExportModal.ExportProgress.Step := 1;
  ExportModal.ExportProgress.Min  := 0;
  ExportModal.ExportProgress.Max  := PlayingWindow[1].VTMP.Positions.Length +
    ((PlayingWindow[1].VTMP.Positions.Length - PlayingWindow[1].VTMP.Positions.Loop) * ExportLoops);
  ExportModal.Show;

  // Prepare memory stream and buffer
  TMPFileName := FileName+'.tmp';
  TMPFileStream := TFileStream.Create(TMPFileName, fmCreate);
  AudioBufferSize := BufferLength * (SampleBit div 8) * NumberOfChannels;
  SetLength(Buf, AudioBufferSize);
  Chip1Position := 0;
  Chip2Position := 0;

  i := 0;
  ExportStarted  := True;
  ExportFinished := False;
  repeat

    if ExportLoops = 0 then
      LoopAllowed := False;

    // Cancel export by Esc or Application Exit
    if MainForm.VTExit or (GetAsyncKeyState(VK_ESCAPE) and $8001 <> 0) then
    begin
      TMPFileStream.Free;
      ExportModal.Free;
      DeleteFile(TMPFileName);

      MainForm.SetEmulatingChip(prevEmulatingChip);
      Set_Optimization(prevOptForQuality);
      SetSampleRate(prevSampleRate);
      SetBitRate(prevBitRate);
      SetNChans(prevNumChans);
      LoopAllowed := prevLoopAllowed;

      ExportStarted := False;
      if MainForm.VTExit then
        Exit;

      EnableControls;
      PlayingWindow[1].SelectPosition2(0);
      if PlayingWindow[2] <> nil then PlayingWindow[2].SelectPosition2(0);

      Exit;
    end;

    // Reset buffer
    FillChar(Buf[0], AudioBufferSize, 0);

    // Make buffer and save to memory stream
    MakeBuffer(@Buf[0]);

    // Save buffer
    if BuffLen < BufferLength then
      TMPFileStream.Write(Buf[0], BuffLen * (SampleBit div 8) * NumberOfChannels)
    else
      TMPFileStream.Write(Buf[0], AudioBufferSize);

    if ExportFinished then
      Break;

    // Process messages each 3 loops
    Inc(i);
    if i = 2 then
    begin
      Application.ProcessMessages;
      i := 0;
    end;

    // Change positions
    if Chip1Position <> PlVars[1].CurrentPosition then
    begin
      Chip1Position := PlVars[1].CurrentPosition;
      PlayingWindow[1].SelectPosition2(Chip1Position);
      PlayingWindow[1].PageControl1.Repaint;
      ExportModal.ExportProgress.Position := ExportModal.ExportProgress.Position + 1;
    end;

    if (NumberOfSoundChips = 2) and (Chip2Position <> PlVars[2].CurrentPosition) then
    begin
      Chip2Position := PlVars[2].CurrentPosition;
      PlayingWindow[2].SelectPosition2(Chip2Position);
      PlayingWindow[2].PageControl1.Repaint;
    end;

  until False;

  ExportModal.Free;



  // Save wav
  CreateWaveHeader(TMPFileStream.Size, SampleRate, SampleBit, 2, WaveHeader);

  FileStream := TFileStream.Create(FileName, fmCreate);
  FileStream.Seek(0, soFromBeginning);
  FileStream.Write(WaveHeader, SizeOf(WaveHeader));
  TMPFileStream.Position := 0;
  FileStream.CopyFrom(TMPFileStream, TMPFileStream.Size);

  FileStream.Free;
  TMPFileStream.Free;
  DeleteFile(TMPFileName);


  // Restore chip & audio params
  MainForm.SetEmulatingChip(prevEmulatingChip);
  Set_Optimization(prevOptForQuality);
  SetSampleRate(prevSampleRate);
  SetBitRate(prevBitRate);
  SetNChans(prevNumChans);
  LoopAllowed := prevLoopAllowed;

  // Restore controls
  EnableControls;

  // Set childs positions
  PlayingWindow[1].SelectPosition2(0);
  if PlayingWindow[2] <> nil then PlayingWindow[2].SelectPosition2(0);

  ExportStarted := False;

end;





initialization

  WOEventH := CreateEvent(nil, False, False, nil);
  InitializeCriticalSection(WOCS);

finalization

  DeleteCriticalSection(WOCS);
  CloseHandle(WOEventH);

end.
