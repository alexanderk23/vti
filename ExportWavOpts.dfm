object ExportOptions: TExportOptions
  Left = 422
  Top = 118
  BorderStyle = bsDialog
  Caption = 'Export Options'
  ClientHeight = 334
  ClientWidth = 233
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Chip: TRadioGroup
    Left = 8
    Top = 152
    Width = 217
    Height = 65
    Items.Strings = (
      'AY'
      'YM')
    TabOrder = 0
  end
  object SampleRate: TRadioGroup
    Left = 8
    Top = 8
    Width = 217
    Height = 137
    Items.Strings = (
      '22050 Hz'
      '44100 Hz'
      '48000 Hz'
      '88200 Hz'
      '96000 Hz')
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 224
    Width = 217
    Height = 57
    TabOrder = 2
    object ExportNumLoops: TLabel
      Left = 9
      Top = 23
      Width = 61
      Height = 13
      Caption = 'Repeat loop:'
    end
    object Label1: TLabel
      Left = 144
      Top = 23
      Width = 24
      Height = 13
      Caption = 'times'
    end
    object LpRepeat: TEdit
      Left = 80
      Top = 19
      Width = 41
      Height = 25
      TabOrder = 0
      Text = '0'
    end
    object LoopRepeats: TUpDown
      Left = 121
      Top = 19
      Width = 14
      Height = 25
      Associate = LpRepeat
      Max = 10
      TabOrder = 1
    end
  end
  object Button1: TButton
    Left = 144
    Top = 296
    Width = 75
    Height = 25
    Caption = 'Export'
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 56
    Top = 296
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
    OnClick = Button2Click
  end
end
